require("dotenv").config()
module.exports = 
{
  "development": {
    "username": process.env.DB_USER,
    "password": process.env.DB_PASS,
    "database": process.env.DB_NAME,
    "host": process.env.DB_HOST,
    "dialect": process.env.DB_DIALECT,
    "timezone": process.env.DB_TIMEZONE
  },
  "test": {
    "username": "root",
    "password": "",
    "database": "my_database",
    "host": "localhost",
    "dialect": "mysql",
    "timezone": "+05:30"
  },
  "production": {
    "username": "root",
    "password": "",
    "database": "my_database",
    "host": "localhost",
    "dialect": "mysql"
  }
}
