const fs = require("fs");
const jwt = require("jsonwebtoken");
function logReqRes(fileName) {
  return (req, res, next) => {
    const currentDate = new Date().toLocaleString("en-UK", {
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
      timeZoneName: "short",
    });

    fs.appendFile(
      fileName,
      `${currentDate}: \t ${req.method}: ${req.path}:\t ${req.ip}\n`,
      (err, data) => {
        next();
      }
    );
  };
}

module.exports = {
  logReqRes,
};
