const jwt = require("jsonwebtoken");
const jwtSecretKey = require("./jwtSecretKey");

async function verifyToken(req, res, next) {
  const bearerHeader = await req.headers["authorization"];
  if (typeof bearerHeader !== "undefined") {
    const bearer = bearerHeader.split(" ");
    const token = bearer[1];
    req.token = token;
    try {
      const authData = jwt.verify(req.token, jwtSecretKey);
      if (authData) {
        req.authData = authData;
        console.log("Token Matched");
        return next();
      } else {
        return res.json("Token is not valid");
      }
    } catch (err) {
      return res.json("Token is not valid");
    }
  } else {
    return res.json("Token is not valid");
  }
}

module.exports = verifyToken;
