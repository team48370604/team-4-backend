const { body, validationResult } = require("express-validator");
function validateRestaurant(req, res, next) {
  const validationRules = [
    body("rname")
      .isLength({max:50})
      .withMessage("Give a restaurant name of length upto 50"),
  ];

  // Execute validation rules
  Promise.all(validationRules.map((validation) => validation.run(req))).then(
    () => {
      // Check for validation errors
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }
      // If validation passes, proceed to the next middleware or route handler
      next();
    }
  );
}

module.exports = validateRestaurant;
