const { body, validationResult } = require("express-validator");
function validateBookings(req, res, next) {
  const validationRules = [
    body("restaurant_id")
      .notEmpty()
      .withMessage("Give non empty valid restaurant id")
      .isInt()
      .withMessage("Give a valid restaurant id as integer"),
    body("selected_date")
      .notEmpty()
      .withMessage("Enter a valid non-empty date")
      .isDate()
      .withMessage("Enter a valid date in YYYY-MM-DD format"),
    body("start_time")
      .notEmpty()
      .withMessage("Enter a valid non-empty time ")
      .isTime()
      .withMessage("Enter a valid time in HH:MM format"),
    body("nguests")
      .notEmpty()
      .withMessage("number of guests must be non-empty")
      .isInt({ min: 0, max: 10 })
      .withMessage("number of guests must be int and less than 11"),
  ];

  // Execute validation rules
  Promise.all(validationRules.map((validation) => validation.run(req))).then(
    () => {
      // Check for validation errors
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }
      // If validation passes, proceed to the next middleware or route handler

      next();
    }
  );
}

module.exports = validateBookings;
