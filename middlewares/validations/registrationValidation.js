const { body, validationResult } = require("express-validator");
function validateRegister(req, res, next) {
  const validationRules = [
    body("name")
      .notEmpty()
      .withMessage("name should not be Empty Field")
      .isLength({ min: 1, max: 50 })
      .withMessage("Username is required"),
    body("email")
      .notEmpty()
      .withMessage("Empty email Field")
      .isEmail()
      .withMessage("Invalid email address")
      .isLength({ min: 4, max: 50 })
      .withMessage("Invalid email address length"),
    body("password")
      .isLength({ min: 8 })
      .withMessage("Invalid password length")
      .isAlphanumeric()
      .withMessage("Password must be alphanumeric"),
    body("contact_number")
      .isInt()
      .withMessage("contact_number should be only numbers")
      .isLength(10)
      .withMessage("contact_number must be a 10 digit number"),
  ];

  // Execute validation rules
  Promise.all(validationRules.map((validation) => validation.run(req))).then(
    () => {
      // Check for validation errors
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }
      // If validation passes, proceed to the next middleware or route handler
      next();
    }
  );
}

module.exports = validateRegister;
