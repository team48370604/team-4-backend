const { body, validationResult } = require("express-validator");
function validateLocation(req, res, next) {
  const validationRules = [
    body("location")
      .isLength({ max: 50 })
      .withMessage("Give a non-empty location "),
    body("search_location")
      .isLength({ max: 50 })
      .withMessage("Give a non-empty location "),
  ];

  // Execute validation rules
  Promise.all(validationRules.map((validation) => validation.run(req))).then(
    () => {
      // Check for validation errors
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }
      // If validation passes, proceed to the next middleware or route handler
      next();
    }
  );
}

module.exports = validateLocation;
