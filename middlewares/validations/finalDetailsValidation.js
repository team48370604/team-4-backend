const { body, validationResult } = require("express-validator");
function validateFinalDetails(req, res, next) {
  const validationRules = [
    //     //"restaurant_id": 11,
    //   "selected_date": "2024-02-05",
    //   "nguests":4
    body("booking_id")
      .notEmpty()
      .withMessage("Give a non_empty booking_id")
      .isInt()
      .withMessage("Give a valid bookings id as integer"),
  ];

  // Execute validation rules
  Promise.all(validationRules.map((validation) => validation.run(req))).then(
    () => {
      // Check for validation errors
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }
      // If validation passes, proceed to the next middleware or route handler
      next();
    }
  );
}

module.exports = validateFinalDetails;
