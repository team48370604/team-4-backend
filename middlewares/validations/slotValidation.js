const { body, validationResult } = require("express-validator");
function validateAvailableSlot(req, res, next) {
  const validationRules = [
    body("restaurant_id")
      .notEmpty()
      .withMessage("should be non_empty restaurant_id")
      .isInt()
      .withMessage("Give a valid restaurant id as integer"),
    body("selected_date")
      .notEmpty()
      .withMessage("Enter a non-empty date in YYYY-MM-DD format")
      .isDate()
      .withMessage("Enter a valid date in YYYY-MM-DD format"),
  ];

  // Execute validation rules
  Promise.all(validationRules.map((validation) => validation.run(req))).then(
    () => {
      // Check for validation errors
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }
      // If validation passes, proceed to the next middleware or route handler
      next();
    }
  );
}

module.exports = validateAvailableSlot;
