const { body, validationResult } = require("express-validator");
function validateLogin(req, res, next) {
  const validationRules = [
    body("email")
      .notEmpty()
      .withMessage("email should not be empty")
      .isEmail()
      .withMessage("email should be in correct format")
      .isLength({ min: 4, max: 50 })
      .withMessage("email should be 4 to 50 in length"),
    body("password")
      .isLength({ min: 8})
      .withMessage("email should be min 8")
      .isAlphanumeric()
      .withMessage(
        "Password must be at least 8 characters long and alphanumeric"
      ),
  ];

  // Execute validation rules
  Promise.all(validationRules.map((validation) => validation.run(req))).then(
    () => {
      // Check for validation errors
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }
      // If validation passes, proceed to the next middleware or route handler
      next();
    }
  );
}

module.exports = validateLogin;
