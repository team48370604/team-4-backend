const crypto = require("crypto");
//Password encryption
const getRandomString = function (length) {
    return crypto.randomBytes(Math.ceil(length / 2))
        .toString('hex')//hex conversion
        .slice(0, length); // return the required number of chars
}
// salt is hmac key 
const sha512 = function (password, salt) {
    let hash = crypto.createHmac('sha512', salt);
    // making new hash using password so later can be used for validation
    hash.update(password);
    let value = hash.digest('hex');// to get hash value
    return {
        salt: salt,
        passwordHash: value
    };
}

function saltHashPassword(userPasswrod) {
    var salt = getRandomString(16);
    var passwordData = sha512(userPasswrod, salt);

    return passwordData;
}

// //testing hashPassword
// app.get( "/",(req,res,next)=>{
//     console.log("password: Aman");
//     var encrypt = saltHashPassword("Aman");
//     console.log('Encrypted: '+ encrypt.passwordHash);
//     console.log('Salt'+ encrypt.salt)
// })

module.exports = {
    getRandomString,
    sha512,
    saltHashPassword,

}

