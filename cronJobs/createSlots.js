const moment = require("moment");
const restaurants = require("../models").restaurants;
const fs = require("fs");

const Sequelize = require("sequelize");
const nodeCron = require("node-cron");

nodeCron.schedule(
  "00 05 14 * * *",
  async function () {
    try {
      console.log("running cron job to create slots");

      const restaurantResults = await restaurants.findAll({
        attributes: ["id"],
      });
      const restaurant_ids = restaurantResults
        .map((row) => row.id)
        .sort((a, b) => a - b);
      //console.log(restaurant_ids);

      const slots_array = [];
      const slots_time = [
        "00:00-00:30",
        "00:30-01:00",
        "01:00-01:30",
        "01:30-02:00",
        "02:00-02:30",
        "02:30-03:00",
        "03:00-03:30",
        "03:30-04:00",
        "04:00-04:30",
        "04:30-05:00",
        "05:00-05:30",
        "05:30-06:00",
        "06:00-06:30",
        "06:30-07:00",
        "07:00-07:30",
        "07:30-08:00",
        "08:00-08:30",
        "08:30-09:00",
        "09:00-09:30",
        "09:30-10:00",
        "10:00-10:30",
        "10:30-11:00",
        "11:00-11:30",
        "11:30-12:00",
        "12:00-12:30",
        "12:30-13:00",
        "13:00-13:30",
        "13:30-14:00",
        "14:00-14:30",
        "14:30-15:00",
        "15:00-15:30",
        "15:30-16:00",
        "16:00-16:30",
        "16:30-17:00",
        "17:00-17:30",
        "17:30-18:00",
        "18:00-18:30",
        "18:30-19:00",
        "19:00-19:30",
        "19:30-20:00",
        "20:00-20:30",
        "20:30-21:00",
        "21:00-21:30",
        "21:30-22:00",
        "22:00-22:30",
        "22:30-23:00",
        "23:00-23:30",
        "23:30-00:00",
      ];

      function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
      }

      for (let i = 0; i < restaurant_ids.length; i++) {
        for (let j = 0; j < slots_time.length; j++) {
          let temp_obj = {
            restaurant_id: restaurant_ids[i],
            start_time: slots_time[j].slice(0, 5),
            end_time: slots_time[j].slice(6, 11),
            capacity: getRandomInt(2, 8),
          };
          slots_array.push(temp_obj);
        }
      }
      console.log(slots_array);
      const seedVar = "module.exports = ";
      const slots_json = JSON.stringify(slots_array, null, 2);

      console.log("printed");
      fs.writeFileSync("seedData/slots.js", seedVar + slots_json, (err) => {
        if (err) {
          console.error("Error writing file:", err);
        } else {
          console.log("Sorted IDs have been written to slots.js");
        }
      });
    } catch (err) {
      console.log(err);
    }
  },
  {
    scheduled: true,
    timezone: "Asia/Kolkata",
  }
);
