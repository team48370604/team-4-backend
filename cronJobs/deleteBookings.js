const moment = require("moment");
const bookings = require("../models").bookings;
const { sequelize, Sequelize } = require("../models");
const nodeCron = require("node-cron");


nodeCron.schedule(
  "0 56 12 * * *",
  async function () {
    const t = await sequelize.transaction({
      isolationLevel: Sequelize.Transaction.ISOLATION_LEVELS.READ_COMMITTED, // Set isolation level to READ COMMITTED prevents dirty reads
    });
  
    try{
    console.log("running cron job to delete bookings");    
    await bookings.destroy({
        where: {
          book_date: {
            [Sequelize.Op.lte]: moment().format("YYYY-MM-DD HH:mm:ss")
          }
        }
      },{
        transaction:t,
      });
      t.commit();
      
    console.log('Deleted old data successfully');
}catch(err){
  t.rollback();
    console.log(err);
}
  },
  { 
    scheduled: true, 
    timezone: "Asia/Kolkata" 
}
);

