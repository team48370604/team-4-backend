const moment = require("moment");
const inventory = require("../models").inventory;
const slots = require("../models").slots;
const restaurants = require("../models").restaurants;
const { sequelize, Sequelize } = require("../models");
const nodeCron = require("node-cron");
const fs = require("fs");

nodeCron.schedule(
  "00 58 12 * * *",
  async function () {
    const t = await sequelize.transaction({
      isolationLevel: Sequelize.Transaction.ISOLATION_LEVELS.READ_COMMITTED, // Set isolation level to READ COMMITTED prevents dirty reads
    });
  
    try {
      console.log(
        "running cron job to delete old inventory and create the new inventory slots"
      );

      await inventory.destroy({
        where: {
          slot_time: {
            [Sequelize.Op.lt]: moment().format("YYYY-MM-DD"),
          },
        },
      },{
        transaction:t
      });

      const numDays = 5;

      const distinctTimes = await inventory.findAll({
        attributes: [
          [Sequelize.fn("DISTINCT", Sequelize.col("slot_time")), "slot_time"],
        ],
      },{
        transaction:t
      });

      const distinctDates = distinctTimes.map((time) => time.slot_time);

      const distinctDates2 = [];
      for (let k = 0; k < numDays; k++) {
        const currentDate = moment();
        const newDate = currentDate.add(k, "days");
        const formattedDate = String(newDate.format("YYYY-MM-DD"));
        distinctDates2.push(formattedDate);
      }

      if (JSON.stringify(distinctDates) === JSON.stringify(distinctDates2)) {
        console.log("No changes");
        return;
      } else if (distinctDates.length === 0) {
        console.log("running cron job to make inventory for five days");
        const restaurantResults = await restaurants.findAll({
          attributes: ["id"],
        },{
          transaction:t
        });
        const restaurant_ids = restaurantResults
          .map((row) => row.id)
          .sort((a, b) => a - b);
        console.log(restaurant_ids);

        const inventory_array = [];

        for (let i = 0; i < restaurant_ids.length; i++) {
          const slotResults = await slots.findAll({
            attributes: ["id", "capacity"],
            where: { restaurant_id: restaurant_ids[i] },
          },{
            transaction:t
          });
          const slots_info = slotResults.map((row) => ({
            slot_id: row.id,
            capacity: row.capacity,
          }));
          for (let k = 0; k < numDays; k++) {
            const currentDate = moment();
            const newDate = currentDate.add(k, "days");
            //console.log(k, newDate);
            const formattedDate = newDate.format("YYYY-MM-DD");

            for (let j = 0; j < slots_info.length; j++) {
              let temp_obj = {
                restaurant_id: restaurant_ids[i],
                slot_id: slots_info[j]["slot_id"],
                quantity: slots_info[j]["capacity"],
                slot_time: formattedDate,
              };
              inventory_array.push(temp_obj);
            }
          }
        }
        await inventory.bulkCreate(inventory_array);
        t.commit();
        console.log("added to the inventory table");
      } else {
        // console.log(distinctTimeValues);
        // const distinctDates = [
        //   "2024-02-14",
        //   "2024-02-15",
        //   "2024-02-16",
        //   "2024-02-17",
        //   "2024-02-18",
        // ];

        // to see missing number of days in the inventory
        // const currentDate = moment();
        // const countDistinctDates = distinctDates.length;
        // let additionalDays = numDays - countDistinctDates;

        // for (let i = 0; i < countDistinctDates; i++) {
        //   const itrDate = moment(distinctDates[i]);
        //   if (itrDate.isBefore(currentDate, "day")) {
        //     additionalDays++;
        //   }
        // }

        const inventory_array = [];

        const restaurantResults = await restaurants.findAll({
          attributes: ["id"],
        },{
          transaction:t
        });
        const restaurant_ids = restaurantResults
          .map((row) => row.id)
          .sort((a, b) => a - b);
        console.log(restaurant_ids);

        let slots_info_complete = []; // this will have 20 arrays of slots json obj
        for (let i = 0; i < restaurant_ids.length; i++) {
          const slotResults = await slots.findAll({
            attributes: ["id", "capacity"],
            where: { restaurant_id: restaurant_ids[i] },
          },{
            transaction:t
          });
          const slots_info = slotResults.map((row) => ({
            slot_id: row.id,
            capacity: row.capacity,
          }));
          slots_info_complete.push(slots_info);
        }
        console.log(slots_info_complete);

        for (let k = 0; k < numDays; k++) {
          const currentDate = moment();
          const newDate = currentDate.add(k, "days");
          const formattedDate = String(newDate.format("YYYY-MM-DD"));
          console.log(distinctDates, formattedDate);
          console.log(distinctDates.includes(formattedDate));
          if (!distinctDates.includes(formattedDate)) {
            console.log("inside");
            for (let i = 0; i < restaurant_ids.length; i++) {
              const slots_info_length = slots_info_complete[i].length;
              for (let j = 0; j < slots_info_length; j++) {
                let temp_obj = {
                  restaurant_id: restaurant_ids[i],
                  slot_id: slots_info_complete[i][j]["slot_id"],
                  quantity: slots_info_complete[i][j]["capacity"],
                  slot_time: formattedDate,
                };
                j++;
                inventory_array.push(temp_obj);
              }
            }
          }
        }
        // const inventory_json = JSON.stringify(inventory_array, null, 2);
        // fs.writeFileSync("seedData/inventory.js", inventory_json, (err) => {
        //   if (err) {
        //     console.error("Error writing file:", err);
        //   } else {
        //     console.log("Inventory written on file.js");
        //   }
        // });
        await inventory.bulkCreate(inventory_array);
        t.commit();
        console.log("added missing days to the inventory table");
      }
      console.log(distinctDates,distinctDates2);
    } catch (err) {
      t.rollback();
      console.log(err);
    }
  },
  {
    scheduled: true,
    timezone: "Asia/Kolkata",
  }
);
