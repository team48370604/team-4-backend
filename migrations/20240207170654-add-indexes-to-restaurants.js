"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.sequelize.query(
      "CREATE INDEX idx_location ON restaurants(location);"
    );

    await queryInterface.sequelize.query(
      "CREATE INDEX idx_rname ON restaurants(name);"
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.sequelize.query(
      "DROP INDEX idx_location ON restaurants;"
    ); // specify columnn or specify index name
    await queryInterface.sequelize.query(
      "DROP INDEX idx_rname ON restaurants;"
    );
  },
};
