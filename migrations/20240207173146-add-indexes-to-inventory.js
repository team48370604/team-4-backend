'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {

    await queryInterface.sequelize.query(
      'CREATE INDEX idx_inventory_restaurant_id_slot_time_quantity ON inventory (restaurant_id, slot_time, quantity);'
    );
    await queryInterface.sequelize.query(
      'CREATE INDEX idx_inventory_id_restaurant_id ON inventory (id, restaurant_id);'
    );
    await queryInterface.sequelize.query(
      'CREATE INDEX idx_slot_id_slot_time ON inventory (slot_id, slot_time);'
    );
  },

  async down (queryInterface, Sequelize) {
    // await queryInterface.sequelize.query(
    //   'DROP INDEX idx_inventory_slot_time_date_restaurant_id ON inventory; '
    // );
    
  }
};
