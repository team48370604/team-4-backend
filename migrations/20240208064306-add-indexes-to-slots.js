'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.sequelize.query(
      'CREATE INDEX idx_restaurant_id_start_time ON slots ( restaurant_id, start_time );'
    );
    await queryInterface.sequelize.query(
      'CREATE INDEX idx_id_restaurant_id ON slots ( id, restaurant_id );'
    );
  },

  async down (queryInterface, Sequelize) {
    // await queryInterface.sequelize.query(
    //   'DROP INDEX idx_slots_slot_time_date_restaurant_id ON slots ;'
    // );    
   // if you want to drop them write date constraint first while making index of this file. the first one
    
  }
};
