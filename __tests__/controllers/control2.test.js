const handleLogin = require('../../controllers/handleLogin');
const  {customers}  = require('../../models');
const jwt = require('jsonwebtoken');
const jwtSecretKey = require('../../middlewares/jwtSecretKey');

jest.mock('../../models');
//jest.mock('jsonwebtoken');
jest.mock('jsonwebtoken', () => ({
  sign: jest.fn(),
}));

jest.mock('jsonwebtoken', () => ({
  ...jest.requireActual('jsonwebtoken'),
  verify: jest.fn().mockReturnValue({ verified: 'true' }),
}));

describe('handleLogin', () => {
  let req, res, next;

  beforeEach(() => {
    req = {
      body: {
        email: 'test@example.com',
        password: 'password123',
      },
    };
    res = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    next = jest.fn();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  test('should return 400 if email or password is missing', async () => {
    delete req.body.email;
    await handleLogin(req, res);
    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledWith({ message: 'Bad request! all fields are required!' });
  });

  test('should return "User does not exist!!!" if user not found', async () => {
    customers.findOne.mockResolvedValue(null);
    await handleLogin(req, res);
    expect(res.json).toHaveBeenCalledWith({ message: 'User does not exist!!!' });
  });

  test('should return "Wrong password!" if password is incorrect', async () => {
    const user = { id: 1, name: 'Test User', email: 'test@example.com', password: 'hashedPassword', salt: 'randomSalt' };
    customers.findOne.mockResolvedValue(user);
    await handleLogin(req, res);
    expect(res.json).toHaveBeenCalledWith({ message: 'Wrong password!' });
  });

  // test('should return token if login successful', async () => {
  //   const user = { 
  //     id: 1, 
  //     name: 'Test User', 
  //     email: 'test@example.com', 
  //     password: 'hashedPassword', 
  //     salt: 'randomSalt' 
  //   };
  //   // Mock the database query to return the user
  //   customers.findOne.mockResolvedValue(user);
  //   // Mock the jwt sign function to return a token
  //   jwt.sign.mockImplementation((user, jwtSecretKey, options, callback) => {
  //     callback(null, 'token');
  //   });
  //   const verify = jest.spyOn(jwt, jwtSecretKey);
  //   verify.mockImplementation(() => () => ({ verified: 'true' }));

  //   // Call the handleLogin function
  //   await handleLogin(req, res);

  //   // Expect the response to contain the token
  //   expect(res.json).toHaveBeenCalledWith({ token: 'token' });
  // });

  test('should handle internal server error', async () => {
    customers.findOne.mockRejectedValue(new Error('Database error'));
    await handleLogin(req, res);
    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.json).toHaveBeenCalledWith({ message: 'Internal Server Error' });
  });
});
