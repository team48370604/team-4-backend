const handleFinalDetails  = require("../../controllers/handleFinalDetails");
const { bookings } = require("../../models");

// Mock Sequelize
jest.mock("../../models");

const mockRequest = (body) => ({
  body,
});

const mockResponse = () => {
  const res = {};
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);
  return res;
};

describe("handleFinalDetails", () => {
  let req, res;

  beforeEach(() => {
    req = mockRequest({});
    res = mockResponse();
    jest.clearAllMocks();
  });

  it("should return booking details for a valid booking ID", async () => {
    const mockBooking = { id: 1, /* other booking details */ };
    bookings.findByPk.mockResolvedValueOnce(mockBooking);

    req = mockRequest({ id: 1 });

    await handleFinalDetails(req, res);

    expect(res.json).toHaveBeenCalledWith(mockBooking);
  });

  it("should return 'Booking not found' for an invalid booking ID", async () => {
    bookings.findByPk.mockResolvedValueOnce(null);

    req = mockRequest({ id: 999 });

    const expectedResponse = "Booking not found";

    await handleFinalDetails(req, res);

    expect(res.json).toHaveBeenCalledWith({message:expectedResponse});
  });

  it("should handle internal server errors correctly", async () => {
    const internalError = new Error("Internal server error");
    bookings.findByPk.mockRejectedValueOnce(internalError);

    req = mockRequest({ id: 1 });

    const expectedResponse = { error: "Internal Server Error" };

    await handleFinalDetails(req, res);

    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.json).toHaveBeenCalledWith(expectedResponse);
  });
});
