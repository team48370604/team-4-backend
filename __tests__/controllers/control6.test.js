const handleAvailableSlotB = require("../../controllers/handleAvailableSlotsB");

const { customers, inventory, slots, bookings } = require("../../models");

const moment = require("moment");
const { Sequelize } = require("sequelize");


//jest.mock('moment', () => () => ({ format: jest.fn(), subtract: jest.fn(), add: jest.fn() }));
jest.mock('../../models', () => ({
  inventory: { findAll: jest.fn() },
  slots: { findAll: jest.fn() },
}));

describe("handleAvailableSlotB", () => {
  let req;
  let res;

  beforeEach(() => {
    req = {
      body: { restaurant_id: 1, selected_date: '2024-02-20', nguests: 4 },
      authData: { userDetails: { cid: 1 } }
    };

    res = { json: jest.fn(), status: jest.fn().mockReturnThis() };
  });
  
jest.mock('sequelize', () => {
  const mSequelize = {
    authenticate: jest.fn(),
    define: jest.fn(),
    fn: jest.fn(),
    literal: jest.fn(),
    col:  jest.fn(),
    Op: { and: jest.fn() }, // Mock any other Sequelize methods or properties you're using
  };
  return {
    Sequelize: jest.fn(() => mSequelize), // Mock Sequelize constructor
  };
});

  afterEach(() => {
    jest.resetAllMocks();
  });

  it("should return an error if the selected date is in the past", async () => {
    const req = {
      query: {
        page: 1,
        limit: 5,
      },
      body: {
        restaurant_id: 1,
        selected_date: '2022-01-01',
        nguests: 4,
      },
    };
    const res = { json: jest.fn() };


    await handleAvailableSlotB(req, res);

    expect(res.json).toHaveBeenCalledWith({ message: "Wrong Date Book in future" });
  });
  it("should return an error if there are no slots available for the selected date", async () => {
    const req = {
      query: {
        page: 1,
        limit: 5,
      },
      body: {
        restaurant_id: 1,
        selected_date: '2025-01-01',
        nguests: 10,
      },
    };
    const mockSlot = [  ]
    const mockInv = []
  
    slots.findAll.mockResolvedValue(mockSlot);
    inventory.findAll.mockResolvedValue(mockInv);
  
    await handleAvailableSlotB(req, res);
  
    expect(res.json).toHaveBeenCalledWith({ message: "No slots are available for that day" });
  });
  
  it("should return the available slots for the selected date", async () => {
    const req = {
      query: {
        page: 1,
        limit: 5,
      },
      body: {
        restaurant_id: 1,
        selected_date: '2025-01-01',
        nguests: 4,
      },
    };
  
    const res = { json: jest.fn(), status: jest.fn().mockReturnThis() };
  
    const slotResults = [
      { start_time: "16:00:00", end_time: "17:00:00" },
      { start_time: "18:00:00", end_time: "19:00:00" },
    ];
    const mockInv = [{
      slot_time: "16:00:00",
      quanity: 1
    },{
      slot_time: "18:00:00",
      quanity: 1
  },]
  
    slots.findAll.mockResolvedValue(slotResults);
    inventory.findAll.mockResolvedValue(mockInv);
  
    await handleAvailableSlotB(req, res);
  
    expect(res.json).toHaveBeenCalledWith({
      checkdate: "2025-01-01",
      page: 1,
      limit: 5,
      offset: 0,
      totalPages: 1,
      remainingSlots: ["16:00-17:00", "18:00-19:00"],
    });
  });

});