
const  handleSelectRestaurant  = require("../../controllers/handleSelectRestaurant");

const { restaurants } = require("../../models");

jest.mock("../../models");
// Mocking the request and response objects
const req = {
  query: {},
  body: { rname: 'testRestaurant' }
};
const res = {
  json: jest.fn(),
  status: jest.fn(() => res),
};

// Mocking the findAll and count methods of the restaurants model
jest.mock('../../models', () => ({
  restaurants: {
    findAll: jest.fn(),
    count: jest.fn(),
  },
}));

describe('handleSelectRestaurant', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should return restaurants based on name with default pagination values', async () => {
    // Mocking the returned values from the database
    const mockResults = [{ id: 1, name: 'testRestaurant' }];
    const mockTotalRecords = 1;
    restaurants.findAll.mockResolvedValue(mockResults);
    restaurants.count.mockResolvedValue(mockTotalRecords);

    await handleSelectRestaurant(req, res);

    expect(restaurants.findAll).toHaveBeenCalledWith({
      where: { name: 'testRestaurant' },
      offset: 0,
      limit: 5,
    });
    expect(restaurants.count).toHaveBeenCalledWith({
      where: { name: 'testRestaurant' },
    });
    expect(res.json).toHaveBeenCalledWith({
      page: 1,
      limit: 5,
      offset: 0,
      totalPages: 1,
      results: mockResults,
    });
  });

  it('should handle pagination properly', async () => {
    // Setting custom pagination values in the request
    req.query.page = '2';
    req.query.limit = '3';
    const mockResults = [{ id: 2, name: 'testRestaurant' }];
    const mockTotalRecords = 1;
    restaurants.findAll.mockResolvedValue(mockResults);
    restaurants.count.mockResolvedValue(mockTotalRecords);

    await handleSelectRestaurant(req, res);

    expect(restaurants.findAll).toHaveBeenCalledWith({
      where: { name: 'testRestaurant' },
      offset: 3,
      limit: 3,
    });
    expect(restaurants.count).toHaveBeenCalledWith({
      where: { name: 'testRestaurant' },
    });
    expect(res.json).toHaveBeenCalledWith({
      page: 2,
      limit: 3,
      offset: 3,
      totalPages: 1,
      results: mockResults,
    });
  });

  it('should handle database errors gracefully', async () => {
    // Mocking the database query to throw an error
    restaurants.findAll.mockRejectedValue(new Error('Database error'));

    await handleSelectRestaurant(req, res);

    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.json).toHaveBeenCalledWith({ error: 'Internal Server Error' });
  });
});
