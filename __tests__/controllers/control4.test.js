const  handleLocations  = require("../../controllers/handleLocations");
const { restaurants } = require("../../models");
const Sequelize = require("sequelize");
jest.mock("../../models");

const mockRequest = (body, query) => ({
  body,
  query,
});

const mockResponse = () => {
  const res = {};
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);
  return res;
};

describe("handleLocations", () => {
  let req, res;

  beforeEach(() => {
    req = mockRequest({}, {});
    res = mockResponse();
    jest.clearAllMocks();
  });

  it("should return distinct locations when searchLocation is not provided", async () => {
    const mockResults = [
      { 
        location: "Location1",
      }, 
      { 
        location: "Location2",
      },
      { 
        location: "Location1",
      }, 
      { 
        location: "Location2",
      },
      { 
        location: "Location1",
      }, 
      { 
        location: "Location2",
      },
    ];
    restaurants.findAll.mockResolvedValueOnce(mockResults);

    await handleLocations(req, res);
    

    expect(res.json).toHaveBeenCalledWith({
      page: 1,
      limit: 5,
      offset: 0,

      results: mockResults,
    });
  });

  it("should return filtered locations when searchLocation is provided", async () => {
    req.body = {
      search_location: "SearchTerm",
    };

    const mockResults = [{ location: "FilteredLocation" }];
    restaurants.findAll.mockResolvedValueOnce(mockResults);

    await handleLocations(req, res);

    expect(res.json).toHaveBeenCalledWith({
      page: 1,
      limit: 5,
      offset: 0,
      
      results: mockResults,
    });
  });

  it("should handle internal server errors correctly", async () => {
    const internalError = new Error("Internal server error");
    restaurants.findAll.mockRejectedValueOnce(internalError);

    await handleLocations(req, res);

    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.json).toHaveBeenCalledWith({ error: "Internal Server Error" });
  });
});