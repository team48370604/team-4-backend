const  handleRestaurants = require("../../controllers/handleRestaurants");
const { restaurants } = require("../../models");

jest.mock("../../models");
// Mocking the request and response objects
const req = {
  query: {},
  body: { location: 'testLocation' }
};
const res = {
  json: jest.fn(),
  status: jest.fn(() => res),
};

// Mocking the findAll and count methods of the restaurants model
jest.mock('../../models', () => ({
  restaurants: {
    findAll: jest.fn(),
    count: jest.fn(),
  },
}));

describe('handleRestaurants', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should return restaurants based on location with default pagination values', async () => {
    // Mocking the returned values from the database
    restaurants.findAll.mockResolvedValue([{ name: 'Restaurant 1' }, { name: 'Restaurant 2' }]);
    restaurants.count.mockResolvedValue(10);

    await handleRestaurants(req, res);

    expect(restaurants.findAll).toHaveBeenCalledWith({
      where: { location: 'testLocation' },
      offset: 0,
      limit: 5,
    });
    expect(restaurants.count).toHaveBeenCalledWith({
      where: { location: 'testLocation' },
    });
    expect(res.json).toHaveBeenCalledWith({
      page: 1,
      limit: 5,
      offset: 0,
      totalPages: 2,
      results: [{ name: 'Restaurant 1' }, { name: 'Restaurant 2' }],
    });
  });

  it('should handle error when database query fails', async () => {
    // Mocking the database query to throw an error
    restaurants.findAll.mockRejectedValue(new Error('Database error'));

    await handleRestaurants(req, res);

    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.json).toHaveBeenCalledWith({ error: 'Internal Server Error' });
  });
});
