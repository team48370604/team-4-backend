const handleLocation  = require("../../controllers/handleLocation");
const { restaurants } = require("../../models");

jest.mock("../../models");

describe("handleLocation", () => {
  afterEach(() => {
    jest.clearAllMocks(); // Clear mock calls after each test
  });

  it("should return distinct locations and pagination details when successful", async () => {
    // Mock request and response objects
    const req = { query: { page: 1, limit: 5 }, authData: { userId: 1 } };
    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis(),
    };

    // Mock data for successful execution
    const mockResults = [{ location: "Location1" }, { location: "Location2" }];
    const mockTotalRecords = 10;

    // Mocking the behavior of restaurants.findAll and restaurants.count
    restaurants.findAll.mockResolvedValueOnce(mockResults);
    restaurants.count.mockResolvedValueOnce(mockTotalRecords);

    // Call the controller function
    await handleLocation(req, res);

    // Assertion for successful execution
    expect(res.status).not.toHaveBeenCalled();
    expect(res.json).toHaveBeenCalledWith({
      message: "Success!",
      authData: req.authData,
      page: 1,
      limit: 5,
      offset: 0,
      totalPages: 2,
      results: mockResults,
    });
  });

  it("should return default pagination values when query parameters are missing", async () => {
    // Mock request and response objects with missing query parameters
    const req = { query: {}, authData: { userId: 1 } };
    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis(),
    };

    // Mock data for successful execution
    const mockResults = [{ location: "Location1" }, { location: "Location2" }];
    const mockTotalRecords = 10;

    // Mocking the behavior of restaurants.findAll and restaurants.count
    restaurants.findAll.mockResolvedValueOnce(mockResults);
    restaurants.count.mockResolvedValueOnce(mockTotalRecords);

    // Call the controller function
    await handleLocation(req, res);

    // Assertion for default pagination values
    expect(res.status).not.toHaveBeenCalled();
    expect(res.json).toHaveBeenCalledWith({
      message: "Success!",
      authData: req.authData,
      page: 1, // Default page value
      limit: 5, // Default limit value
      offset: 0, // Default offset value
      totalPages: 2, // Calculated based on default limit and total records
      results: mockResults,
    });
  });

  it("should handle Sequelize query error", async () => {
    // Mock request and response objects
    const req = { query: { page: 1, limit: 5 }, authData: { userId: 1 } };
    const res = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis(),
    };

    // Mock Sequelize query error
    const sequelizeError = new Error("Sequelize query error");
    restaurants.findAll.mockRejectedValueOnce(sequelizeError);

    // Call the controller function
    await handleLocation(req, res);

    // Assertion for handling Sequelize query error
    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.json).toHaveBeenCalledWith({ error: "Internal Server Error" });
  });
});
