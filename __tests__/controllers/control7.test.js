const  handleSavingSlotB  = require("../../controllers/handleSavingSlotB");
const { customers, inventory, slots, bookings } = require("../../models");

jest.mock("../../models");

describe('handleSavingSlotB', () => {
  let req;
  let res;

  beforeEach(() => {
    req = {
      body: { restaurant_id: 1, selected_date: '2024-02-20', start_time: '12:00:00', nguests: 4 },
      authData: { userDetails: { cid: 1 } }
    };

    res = { json: jest.fn(), status: jest.fn().mockReturnThis(1) };
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  test('should return booking_id when successfully saving a slot', async () => {
    const customer = { name: 'Test Customer', contact_number: '1234567890' };
    customers.findByPk.mockResolvedValue(customer);
    slots.findAll.mockResolvedValue([{ id: 1, capacity: 2 }]);
    inventory.findAll.mockResolvedValue([{ quantity: 2 }]);
    bookings.create.mockResolvedValue({ id: 1 });

    await handleSavingSlotB(req, res);

    expect(res.json).toHaveBeenCalledWith({ booking_id: 1 });
  });


  test('should handle case when the restaurant is full for the selected timeslot', async () => {
    const customer = { name: 'Test Customer', contact_number: '1234567890' };
    customers.findByPk.mockResolvedValue(customer);
    slots.findAll.mockResolvedValue([{ id: 1, capacity: 2 }]);
    inventory.findAll.mockResolvedValue([{ quantity: -1 }]);
    
    await handleSavingSlotB(req, res);

    expect(res.json).toHaveBeenCalledWith({ message: 'The restaurant is full for this timeslot choose other slot.' });
  });
  test('should handle case when the selected date is in the past', async () => {
    req.body.selected_date = '2023-02-20';

    await handleSavingSlotB(req, res);

    expect(res.json).toHaveBeenCalledWith({ message: 'Wrong Date Book in future' });
  });

  test('should handle case when no customer found', async () => {
    customers.findByPk.mockResolvedValueOnce(null);

    await handleSavingSlotB(req, res);

    expect(res.json).toHaveBeenCalledWith({ message: 'invalid_customer' });
  });

  test('should handle database error', async () => {
    inventory.findAll.mockRejectedValueOnce(new Error('Database error'));

    await handleSavingSlotB(req, res);

    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.json).toHaveBeenCalledWith({ error: 'Internal Server Error' });
  });
});