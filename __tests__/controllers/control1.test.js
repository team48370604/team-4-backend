const handleRegistration  = require("../../controllers/handleRegistration");

const {customers} = require("../../models");
jest.mock("../../models");

const mockRequest = (body) => ({
  body,
});

const mockResponse = () => {
  const res = {};
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);
  return res;
};

describe("handleRegistration", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should return 400 if required fields are missing", async () => {
    const req = mockRequest({});
    const res = mockResponse();

    await handleRegistration(req, res);

    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledWith({
      message: "Bad request! All fields are required!",
    });
  });

  it("should return 200 and 'User already exists!!!' if the user already exists", async () => {
    const req = mockRequest({
      name: "John Doe",
      email: "john@example.com",
      password: "password123",
      contact_number: "1234567890",
    });
    const res = mockResponse();

    customers.findOne.mockResolvedValueOnce({
      id: 1,
      name: "John Doe",
      email: "john@example.com",
      password: "hashedPassword",
      contact_number: "1234567890",
      salt: "salt",
      createdAt: new Date(),
      updatedAt: new Date(),
    });

    await handleRegistration(req, res);

    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledWith({message:"User already exists!!!"});
  });

  it("should return 201 and 'Register successful' if the user is created", async () => {
    const req = mockRequest({
      name: "New User",
      email: "newuser@example.com",
      password: "newpassword123",
      contact_number: "9876543210",
    });
    const res = mockResponse();

    customers.findOne.mockResolvedValueOnce(null); // Simulating user not existing

    customers.create.mockResolvedValueOnce({
      id: 2,
      name: "New User",
      email: "newuser@example.com",
      password: "hashedNewPassword",
      contact_number: "9876543210",
      salt: "newSalt",
      createdAt: new Date(),
      updatedAt: new Date(),
    });

    await handleRegistration(req, res);

    expect(res.status).toHaveBeenCalledWith(201);
    expect(res.json).toHaveBeenCalledWith({ message: "Register successful" });
  });

  it("should return 500 if an error occurs during registration", async () => {
    const req = mockRequest({
      name: "John Doe",
      email: "john@example.com",
      password: "password123",
      contact_number: "1234567890",
    });
    const res = mockResponse();

    customers.findOne.mockRejectedValueOnce(new Error("Database error"));

    await handleRegistration(req, res);

    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.json).toHaveBeenCalledWith({message:"Internal Server Error"});
  });
});
