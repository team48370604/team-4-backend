const data = 
[
    {
      "id": 1,
      "name": "Guglielma",
      "email": "Aman1@gggggggggggggggg.com",
      "password": "5af1fab9cb91a8d5c4189ffb34a10afae52c84c06ced9598b24f247b5126c4bb9cd612cbf3ddb7b538da8941bf4d132edf976a293743b3ae380feab574a0a9f7",
      "contact_number": 8556856608,
      "salt": "dd99ba7552baef3a",
      "createdAt": "2024-01-30 20:38:33",
      "updatedAt": "2024-01-30 20:38:33"
    },
    {
      "id": 2,
      "name": "Erika",
      "email": "Aman2@gggggggggggggggg.com",
      "password": "5f983147b5023d756e57fa3ec482672a8c98cdbc89b17d7a99c3025966323785e7d6dca7ad36ea583d13268587f304ea7f17d4926e24eab9f31d8127b37ea190",
      "contact_number": 8556856608,
      "salt": "b7a380df11460345",
      "createdAt": "2024-01-30 20:40:17",
      "updatedAt": "2024-01-30 20:40:17"
    },
    {
      "id": 3,
      "name": "Torrin",
      "email": "Aman3@gggggggggggggggg.com",
      "password": "2753747a161d61c93a37a0b2571aaa4b9d4f00f7db6fca2ae77aacab935e2c0a650de7c677d7beb1fc89ff968c2dda625b1287c5347cea3d9c8cd276ab9ac280",
      "contact_number": 8556856608,
      "salt": "626f8c0d4ce78d2b",
      "createdAt": "2024-01-30 20:40:21",
      "updatedAt": "2024-01-30 20:40:21"
    },
    {
      "id": 4,
      "name": "Jacklyn",
      "email": "Aman4@gggggggggggggggg.com",
      "password": "8eb1f81270eeeed793b18d3c908d75975f889db5eaee8c95b685ad2fc8523a5eb7768fe41a6f4fc53bab8ff5d6fe488629186a271f8e7c657c9327eafd2620da",
      "contact_number": 8556856608,
      "salt": "d3e7d607ef7340ca",
      "createdAt": "2024-01-30 20:40:24",
      "updatedAt": "2024-01-30 20:40:24"
    },
    {
      "id": 5,
      "name": "Ulrich",
      "email": "Aman5@gggggggggggggggg.com",
      "password": "112ed9f087b25f831ed597a8da1d6e835748ece2c3b31c75c6a6af1acdcb2039a749fbe07bcde141505e08086faa6db8dfc52efa5c23a444e3ac95d50f42da69",
      "contact_number": 8556856608,
      "salt": "e5f6cc252d4f9609",
      "createdAt": "2024-01-30 20:40:28",
      "updatedAt": "2024-01-30 20:40:28"
    },
    {
      "id": 6,
      "name": "Forster",
      "email": "Aman6@gggggggggggggggg.com",
      "password": "bab2f6bd6caeb607e04be5397f2bab5ba4e7467dc162b0ffc8586f31684205a978a03dfffe31f9c19d696b7fd00d169364f5e525f1624aa9522acf735c84ea69",
      "contact_number": 8556856608,
      "salt": "af36cb7c0311b8d8",
      "createdAt": "2024-01-30 20:40:31",
      "updatedAt": "2024-01-30 20:40:31"
    },
    {
      "id": 7,
      "name": "Abbe",
      "email": "Aman7@gggggggggggggggg.com",
      "password": "d311003dcc438e35e5ea5f74a2940a49ba38b16bd2ab1d37bcf8f946884b834f1d86f580112a346f2fcf55c14b96b5085c28831c791a24dcf373480967a7d926",
      "contact_number": 8556856608,
      "salt": "24dbea13cc537364",
      "createdAt": "2024-01-30 20:40:34",
      "updatedAt": "2024-01-30 20:40:34"
    },
    {
      "id": 8,
      "name": "Terrie",
      "email": "Aman8@gggggggggggggggg.com",
      "password": "9fb5afd7a07d7a55c1c95bd2fdd066775b615619ee7bc5447c5b7f33eae1fa789bde0e9e63404e67345de62e034379fd4b01d928f95c9c212d923b2c1495f569",
      "contact_number": 8556856608,
      "salt": "0a4bd5f9eaaf8d37",
      "createdAt": "2024-01-30 20:40:38",
      "updatedAt": "2024-01-30 20:40:38"
    },
    {
      "id": 9,
      "name": "Inglis",
      "email": "Aman9@gggggggggggggggg.com",
      "password": "98521016d5b67ab7c95abbdceb4a33feb566fb5987a571b868625a3244a465a19571003b15824f24cd656414fdfda4232e631c5ee9548171beb1f0964e5e42b1",
      "contact_number": 8556856608,
      "salt": "85234aa2ed2004cc",
      "createdAt": "2024-01-30 20:40:41",
      "updatedAt": "2024-01-30 20:40:41"
    },
    {
      "id": 10,
      "name": "Myrah",
      "email": "Aman10@gggggggggggggggg.com",
      "password": "c40326d7706ade1767d4d19333f877cd7d5b2d38d027d08cf18e31a1a59921e61b1c35e2d982bca0e91e8330f9b79396046f7ef9a3757ba7c97b125a4bd95404",
      "contact_number": 8556856608,
      "salt": "f73ac9b300462b4c",
      "createdAt": "2024-01-30 20:40:45",
      "updatedAt": "2024-01-30 20:40:45"
    },
    {
      "id": 11,
      "name": "Elinore",
      "email": "Aman11@gggggggggggggggg.com",
      "password": "4b3219622983ad73b3d388aa806b06e26bee9d3013f64bfcbbbd4d22832cc8bfedb7ec309a0d174cb248177ffa57e9d4a5448a88fc0ff7e50d360349329730e7",
      "contact_number": 8556856608,
      "salt": "6c919199865dc1ae",
      "createdAt": "2024-01-30 20:40:49",
      "updatedAt": "2024-01-30 20:40:49"
    },
    {
      "id": 12,
      "name": "Harv",
      "email": "Aman12@gggggggggggggggg.com",
      "password": "3c9074da4a77f446d69c580d8286af2f44e09a5304646dbc89b442b343f73e2e4f81376130f5502babc018784c40730f371f8867f09a2d151dddced101f2ca49",
      "contact_number": 8556856608,
      "salt": "0faef4371a2db14a",
      "createdAt": "2024-01-30 20:40:53",
      "updatedAt": "2024-01-30 20:40:53"
    },
    {
      "id": 13,
      "name": "Neely",
      "email": "Aman13@gggggggggggggggg.com",
      "password": "ba570198d95007e467c5a7e567a1f4570a9c9014df75b1c6e6ea7300d208762b83081c6efbd8ae85284898c34097af729c852670a2f7302a5b35093a80c9946a",
      "contact_number": 8556856608,
      "salt": "8809f4d57be41e39",
      "createdAt": "2024-01-30 20:40:57",
      "updatedAt": "2024-01-30 20:40:57"
    },
    {
      "id": 14,
      "name": "Starlene",
      "email": "Aman14@gggggggggggggggg.com",
      "password": "354a05eb6b8c2695994bbee699d75e2c21872bac7e5c6183cea95fdbf84e0dbe765dd38bc31e9575a530299cb226d9a794e327fe387f65c39c558cd307e75b79",
      "contact_number": 8556856608,
      "salt": "247ca968ae93076e",
      "createdAt": "2024-01-30 20:41:01",
      "updatedAt": "2024-01-30 20:41:01"
    },
    {
      "id": 15,
      "name": "Barty",
      "email": "Aman15@gggggggggggggggg.com",
      "password": "4de72085cd1a5fed22fe4dd6aa037410fbbcf186cf2b45466a3e5642b2627e46eee75c3e901f1dc0f5ac1d86b50b986cf59d7b327ceb98250a354b5e2375e836",
      "contact_number": 8556856608,
      "salt": "3d3412b94f6fa2ad",
      "createdAt": "2024-01-30 20:41:04",
      "updatedAt": "2024-01-30 20:41:04"
    },
    {
      "id": 16,
      "name": "Gretta",
      "email": "Aman16@gggggggggggggggg.com",
      "password": "06a538c3e40aa2ecfefef7e8382e233b341498e8090fdddd917bc2a74f16daf2cb579b41020f925d02db47d65013eff9ea80fe9dfca9ab2fc71a198e59d95e6b",
      "contact_number": 8556856608,
      "salt": "c556a564ed86b063",
      "createdAt": "2024-01-30 20:41:08",
      "updatedAt": "2024-01-30 20:41:08"
    },
    {
      "id": 17,
      "name": "Marj",
      "email": "Aman17@gggggggggggggggg.com",
      "password": "4d903b1fbfc5f36a8efa7238202ac14dc3690165922335475228d768ef65837f6daf3b1ca3a1d95e8f61361658e716982ee58916ae7a1ecc996a756354a29634",
      "contact_number": 8556856608,
      "salt": "a6bf6d39923f8944",
      "createdAt": "2024-01-30 20:41:12",
      "updatedAt": "2024-01-30 20:41:12"
    },
    {
      "id": 18,
      "name": "Moishe",
      "email": "Aman18@gggggggggggggggg.com",
      "password": "2f9e82df9a32c9ea7faf7c7a3d34df94e8a14ea6da88cd3cb865e447f2e13d1a00d929633149a22673ab75a646868fe7c108cbb5e27cf714c945b90114393678",
      "contact_number": 8556856608,
      "salt": "ccc1801c1f7971da",
      "createdAt": "2024-01-30 20:41:14",
      "updatedAt": "2024-01-30 20:41:14"
    },
    {
      "id": 19,
      "name": "Kerwin",
      "email": "Aman19@gggggggggggggggg.com",
      "password": "fc95792cd644c9d08b225311df0b12f1a79500281fd17197ff1c21125124842b2be6f45aa2e7869aa8120acced5785da483d4d626a351809ff2a30834d68e1fa",
      "contact_number": 8556856608,
      "salt": "cfe61e89e9e801e5",
      "createdAt": "2024-01-30 20:41:18",
      "updatedAt": "2024-01-30 20:41:18"
    },
    {
      "id": 20,
      "name": "Lorraine",
      "email": "Aman20@gggggggggggggggg.com",
      "password": "b459170adaef45290edaba0f2e13566b4ba6ca076b1ce477e34bcae7d63d1f045fbbf2d43dac8aaf97f24fc1c49bd6bbf7d1d998d7fd45a6504d0fede1c73a9b",
      "contact_number": 8556856608,
      "salt": "e377b9e8ebbf6860",
      "createdAt": "2024-01-30 20:41:22",
      "updatedAt": "2024-01-30 20:41:22"
    }
  ]
  
module.exports = data;