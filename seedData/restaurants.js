const data = [
  {
    "name": "Skimia",
    "image": "https://washington.org/sites/default/files/Sofitel-Opaline-Restaurant-Terrace-Night_E996D460-48C3-4618-A61E678C56BC81B2_51467d8a-9aab-42ab-90b7b2890c15035a.jpg",
    "location": "Jilong",
    "cuisine_type": "American",
    "createdAt": new Date("2024-01-30T20:45:00"),
    "updatedAt": new Date("2024-01-30T20:45:00")
  },
  {
    "name": "Feedfish",
    "image": "https://i.pinimg.com/originals/a4/56/1e/a4561e967fd90fc116d9eea82e8a095f.jpg",
    "location": "Jilong",
    "cuisine_type": "Mexican",
    "createdAt": new Date("2024-01-30T20:45:05"),
    "updatedAt": new Date("2024-01-30T20:45:05")
  },
  {
    "name": "Centidel",
    "image": "https://offloadmedia.feverup.com/secretneworleans.co/wp-content/uploads/2021/04/29153103/67731641_354010762202573_20217-1-1024x767.jpg",
    "location": "Jilong",
    "cuisine_type": "Indian",
    "createdAt": new Date("2024-01-30T20:45:10"),
    "updatedAt": new Date("2024-01-30T20:45:10")
  },
  {
    "name": "Devbug",
    "image": "https://media.timeout.com/images/105678849/1372/1029/image.jpg",
    "location": "Jilong",
    "cuisine_type": "Greek",
    "createdAt": new Date("2024-01-30T20:45:15"),
    "updatedAt": new Date("2024-01-30T20:45:15")
  },
  {
    "name": "Blogtag",
    "image": "https://www.stylemotivation.com/wp-content/uploads/2017/06/03_2015_BALTAIR-156.0.0-1600x1066.jpg",
    "location": "Redondos",
    "cuisine_type": "Greek",
    "createdAt": new Date("2024-01-30T20:45:20"),
    "updatedAt": new Date("2024-01-30T20:45:20")
  },
  {
    "name": "Wikivu",
    "image": "https://www.timeoutdoha.com/public/images/2020/12/23/La-Spiga-by-Paper-Moon.jpg",
    "location": "Redondos",
    "cuisine_type": "Indian",
    "createdAt": new Date("2024-01-30T20:45:25"),
    "updatedAt": new Date("2024-01-30T20:45:25")
  },
  {
    "name": "Kayveo",
    "image": "https://www.nj.com/resizer/5em6vG14jyA9VNn42Eih0Pi3Qoc=/1280x0/smart/advancelocal-adapter-image-uploads.s3.amazonaws.com/image.nj.com/home/njo-media/width2048/img/entertainment_impact/photo/haven1jpg-d600a77a6b52a4eb.jpg",
    "location": "Qal‘ah-ye Shahr",
    "cuisine_type": "Redondos",
    "createdAt": new Date("2024-01-30T20:45:30"),
    "updatedAt": new Date("2024-01-30T20:45:30")
  },
  {
    "name": "Fadeo",
    "image": "https://www.goodlifereport.com/wp-content/uploads/2017/06/2011-07-14_DSC_0673.jpg",
    "location": "Redondos",
    "cuisine_type": "Japanese",
    "createdAt": new Date("2024-01-30T20:45:35"),
    "updatedAt": new Date("2024-01-30T20:45:35")
  },
  {
    "name": "Quinu",
    "image": "https://destinationmansfield.com/wp-content/uploads/2019/06/DAV_6318.jpg",
    "location": "Redondos",
    "cuisine_type": "Italian",
    "createdAt": new Date("2024-01-30T20:45:40"),
    "updatedAt": new Date("2024-01-30T20:45:40")
  },
  {
    "name": "Katz",
    "image": "https://blog.etundra.com/wp-content/Media/2021/05/WP-Hero-Image.jpg",
    "location": "Redondos",
    "cuisine_type": "Chinese",
    "createdAt": new Date("2024-01-30T20:45:45"),
    "updatedAt": new Date("2024-01-30T20:45:45")
  },
  {
    "name": "Abata",
    "image": "https://us.cameochina.com/wp-content/uploads/2019/07/Improving-the-Outside-Design-of-Your-Restaurant-6-Top-Tips.jpg",
    "location": "Rio Claro",
    "cuisine_type": "Mexican",
    "createdAt": new Date("2024-01-30T20:45:50"),
    "updatedAt": new Date("2024-01-30T20:45:50")
  },
  {
    "name": "Oyope",
    "image": "https://i.pinimg.com/originals/23/72/ef/2372ef99633859024df32360af7683e6.jpg",
    "location": "Awka",
    "cuisine_type": "Greek",
    "createdAt": new Date("2024-01-30T20:45:55"),
    "updatedAt": new Date("2024-01-30T20:45:55")
  },
  {
    "name": "Quaxo",
    "image": "https://i.pinimg.com/originals/32/ee/f4/32eef4d4726a91e828766b3b635d88f8.jpg",
    "location": "Haikou",
    "cuisine_type": "Indian",
    "createdAt": new Date("2024-01-30T20:46:00"),
    "updatedAt": new Date("2024-01-30T20:46:00")
  },
  {
    "name": "Tanoodle",
    "image": "https://i.pinimg.com/originals/07/06/e2/0706e2d368eb7344bfca507cbf3af7db.jpg",
    "location": "Tarnawatka",
    "cuisine_type": "Chinese",
    "createdAt": new Date("2024-01-30T20:46:05"),
    "updatedAt": new Date("2024-01-30T20:46:05")
  },
  {
    "name": "Kwinu",
    "image": "https://admin.hostingloop.com/wp-content/uploads/2021/03/cheescake-1536x1025.jpg",
    "location": "Siteki",
    "cuisine_type": "American",
    "createdAt": new Date("2024-01-30T20:46:10"),
    "updatedAt": new Date("2024-01-30T20:46:10")
  },
  {
    "name": "Eazzy",
    "image": "https://cdn.vox-cdn.com/thumbor/JShtC4it0vUnoO0XIeyYR32lCLc=/0x0:1000x750/1200x900/filters:focal(420x295:580x455):no_upscale()/cdn.vox-cdn.com/uploads/chorus_image/image/62465404/outside_Web.0.0.jpg",
    "location": "Nuamuzi",
    "cuisine_type": "American",
    "createdAt": new Date("2024-01-30T20:46:15"),
    "updatedAt": new Date("2024-01-30T20:46:15")
  },
  {
    "name": "Agimba",
    "image": "https://i.pinimg.com/originals/67/9a/6f/679a6fde9c2668299c1a77497a73d70e.jpg",
    "location": "'s-Hertogenbosch",
    "cuisine_type": "Mexican",
    "createdAt": new Date("2024-01-30T20:46:20"),
    "updatedAt": new Date("2024-01-30T20:46:20")
  },
  {
    "name": "Roomm",
    "image": "https://th.bing.com/th/id/OIP.uOu1cgH9OCNKT4UFrapvcwAAAA?rs=1&pid=ImgDetMain",
    "location": "Roma",
    "cuisine_type": "French",
    "createdAt": new Date("2024-01-30T20:46:25"),
    "updatedAt": new Date("2024-01-30T20:46:25")
  },
  {
    "name": "Realmix",
    "image": "https://bklyner.com/content/images/bklyner/wp-content/uploads/2020/06/DE2E168B-5311-4007-BB1E-BA5914E31F69-1067x800.jpeg",
    "location": "Litian",
    "cuisine_type": "Thai",
    "createdAt": new Date("2024-01-30T20:46:30"),
    "updatedAt": new Date("2024-01-30T20:46:30")
  },
  {
    "name": "Zooveo",
    "image": "https://i.pinimg.com/originals/9b/f9/b3/9bf9b3feb868c1c22f5090e7cddc62bd.jpg",
    "location": "Karanggedang",
    "cuisine_type": "Chinese",
    "createdAt": new Date("2024-01-30T20:46:35"),
    "updatedAt": new Date("2024-01-30T20:46:35")
  }
];

module.exports = data;
