/*
Restful Services 
author: AmanS21
started: 24-01-2024 13:15
*/

const express = require("express");
const {logReqRes} = require("./middlewares/mw");

const userRouter = require("./routes/routers");
const PORT = process.env.PORT;


const db = require("./models");
const app = express();


//This middleware is built into Express (as of version 4.16.0 and later) 
//and is used to parse incoming requests with JSON payloads.
app.use(express.json())
app.use(logReqRes("logs.txt"));



// Routes
app.use("/",userRouter);


db.sequelize.sync().then((req)=>{
    app.listen(PORT, ()=>{
        console.log(`Connected to mysql and synced via Port: ${PORT}`);
        require("./cronJobs/deleteInventory");
        //require("./cronJobs/deleteBookings");
        //require("./cronJobs/deleteSlots");
        require("./cronJobs/createSlots");
        

    });
});

