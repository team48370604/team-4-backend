module.exports = (sequelize, DataTypes) =>{
  // Define the Inventory model

  const inventory = sequelize.define('inventory', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      validate: {
        notEmpty: true,
        isInt: true,
        
      }
    },
    restaurant_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: true
      },
      references: {
        model: 'restaurants', // The table being referenced (case-sensitive)
        key: 'id', // The column being referenced
        onDelete: 'CASCADE'
      }
    },
    slot_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: true
      },references: {
        model: 'slots', // The table being referenced (case-sensitive)
        key: 'id', // The column being referenced
        onDelete: 'CASCADE'
      }
    },
    slot_time:{
      type: DataTypes.DATEONLY,
      allowNull: false,
      validate: {
        notEmpty: true,
        isDate: true,
      },
    },
    quantity: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    
  }, {
    tableName: 'inventory',
    
  });
return inventory;
}