module.exports = (sequelize, DataTypes) =>{

                                //Table_name_if_not_explicitly_mentioned
const customers = sequelize.define('customers', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true,
      validate: {
        notEmpty: true,
        isInt: true,  
      }
    },
    name: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        notEmpty: true,
        len: [4, 50]
      }
    },
    email: {
      type: DataTypes.STRING(50),
      allowNull: false,
      validate: {
        notEmpty: true,
        isEmail: true,
        len: [6, 50]
      }
    },
    password: {
      type: DataTypes.STRING(128),
      allowNull: false,
      validate: {
        notEmpty: true,
        len:128
      }
    },
    contact_number: {
      type: DataTypes.BIGINT,
      allowNull: false,
      validate: {
        notEmpty: true,
        len: 10,
        isInt: true
      }
    },
    salt: {
      type: DataTypes.STRING(16),
      allowNull: false,
      validate: {
        notEmpty: true,
        len:16

      }
    },
  }, {
    freezeTableName: true, 
     // Assuming you don't want Sequelize to manage timestamps
  });
  return customers;

}
