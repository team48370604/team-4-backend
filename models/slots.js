module.exports = (sequelize, DataTypes) =>{

const slots = sequelize.define('slots', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
      validate: {
        notEmpty: true,
        isInt: true
      }
    },
    restaurant_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: true,
        isInt: true
      },
      references: {
        model: 'restaurants', // The table being referenced (case-sensitive)
        key: 'id', // The column being referenced
        onDelete: 'CASCADE'

      },
    },
    start_time: {
      type: DataTypes.TIME,
      allowNull: false,
      validate: {
        notEmpty: true,
        isDate: true,
      }
    },
    end_time: {
      type: DataTypes.TIME,
      allowNull: false,
      validate: {
        notEmpty: true,
        isDate: true,
      }
    },
    capacity: {
      type: DataTypes.INTEGER,
      defautValue: 6,
      validate: {
        notEmpty: true,
        isInt: true
      }
    },
    
  }, {
    freezeTableName:true,
   
  });

  return slots;}
  