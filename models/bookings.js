module.exports = (sequelize, DataTypes) =>{
    const bookings = sequelize.define('bookings', {
        id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
          validate: {
            notEmpty: true,
            isInt : true,
          }
        },
        slot_id: {
          type: DataTypes.INTEGER,
          allowNull: false,
          validate: {
            notEmpty: true,
            isInt : true,
          },
          references: {
            model: 'slots', // The table being referenced (case-sensitive)
            key: 'id', // The column being referenced
            onDelete: 'CASCADE'
          }
          
        },
        customer_id: {
          type: DataTypes.INTEGER,
          allowNull: false,
          validate: {
            notEmpty: true,
            isInt : true,
          },
          references: {
            model: 'customers', // The table being referenced (case-sensitive)
            key: 'id', // The column being referenced
            onDelete: 'CASCADE'
          }
          
        },
        customer_name: {
          type: DataTypes.STRING(45),
          allowNull: false,
          validate: {
            notEmpty: true,
            len: [4, 50]
          }
        
        },
        contact_number: {
          type: DataTypes.BIGINT,
          allowNull: false,
          validate: {
            notEmpty: true,
            isInt : true,
            len: 10
          }
          
        },
        book_date: {
          type: DataTypes.DATE,
          allowNull: false,
          validate: {
            notEmpty: true,
            isDate: true,
          }
         
        },
        num_guests: {
          type: DataTypes.INTEGER,
          allowNull: false,
          validate: {
            notEmpty: true,
            isInt: true,
            min:0,
            max: 10
          }
          
        },
      }, {
        tableName: 'bookings',
        
      });
      return bookings;
}


