module.exports = (sequelize, DataTypes) => {
  const restaurants = sequelize.define(
    "restaurants",
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        validate: {
          notEmpty: true,
          isInt: true
        },
      },
      name: {
        type: DataTypes.STRING(50),
        allowNull: false,
        validate: {
          notEmpty: true,
          len: [4, 50]
        },
      },
      image: {
        type: DataTypes.STRING(1000),
        allowNull: false,
        validate: {
          notEmpty: true,
          isUrl: true
        },
      },
      location: {
        type: DataTypes.STRING(50),
        allowNull: false,
        validate: {
          notEmpty: true,
          isAlpha: true,
          len: [4, 50]
        },
      },
      cuisine_type: {
        type: DataTypes.STRING(50),
        allowNull: false,
        validate: {
          notEmpty: true,
          isAlpha: true,
          len: [4, 50]
        },
      },
    },
    {
      freezeTableName: true,
      timestamps: true,
    }
  );

  return restaurants;
};
