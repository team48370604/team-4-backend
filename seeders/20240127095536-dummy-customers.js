'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    const seedData = require("../seedData/customers");
    return queryInterface.bulkInsert('customers', seedData);
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('customers', null, {});
  }
};
