'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    const seedData = require("../seedData/slots");
    return queryInterface.bulkInsert('slots', seedData);
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('slots', null, {});
  }
};
