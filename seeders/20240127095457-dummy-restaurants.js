'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    const seedData = require("../seedData/restaurants");
    return queryInterface.bulkInsert('restaurants', seedData);
  },
  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('restaurants', null, {});
  }
};
