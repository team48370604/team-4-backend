FROM node:latest

RUN ln -snf /usr/share/zoneinfo/Asia/Kolkata /etc/localtime && echo Asia/Kolkata > /etc/timezone

WORKDIR /app

ADD . .

RUN npm install

CMD npm start