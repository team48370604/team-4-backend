const customers = require("../models").customers;
const slots = require("../models").slots;
const bookings = require("../models").bookings;
const inventory = require("../models").inventory;
const Sequelize = require("sequelize");
const moment = require("moment");

async function handleSavingSlot(req, res) {
  try {
    let { restaurant_id, selected_date, start_time,  nguests } = req.body;

    const authData = req.authData;
    //console.log(authData);


    // checking customer_id
    const customer_id = req.authData.userDetails.cid;
    const customer = await customers.findByPk(customer_id);
      if (!customer) {
        //throw new Error('Customer not found');
        return res.json({
          message: "invalid_customer",
        });
      }



    // setting dates
    const selected_date2 = moment(
      `${selected_date} ${start_time}`,
      "YYYY-MM-DD HH:mm:ss"
    ).format("YYYY-MM-DD HH:mm:ss");

    start_time = selected_date2.slice(11, 16);

    const currentTime = String(
      moment().format("YYYY-MM-DD HH:mm:ss").slice(11, 16)
    );
    const currentDate = String(moment().format("YYYY-MM-DD"));



    // wrong date entered booking in past  
    if (
      selected_date < currentDate ||
      (selected_date == currentDate && start_time < currentTime)
    ) {
      return res.json({ message: "Wrong Date Book in future" });
    }



    //assuming table has 4 seats
    const tableConsumed = Math.ceil(nguests / 4);


    // finding slots in slot table
    const userSlot = await slots.findAll({
      attributes: ["id", "capacity"],
      where: {
        restaurant_id: restaurant_id,
        start_time: selected_date2,
      },
    });

    if (userSlot.length == 0) {
      return res.json({
        message: "Select valid available slot only",
      });
    }


    // checking all the inventory enteries for remaining quantities
    const restaurantInventory = await inventory.findAll({
      where: { slot_id: userSlot[0].id },
    });
    //console.log(restaurantInventory);




    // if no inventory entry on that slot timings
    if (!restaurantInventory || restaurantInventory.length === 0) {

      await inventory.create({
        restaurant_id: restaurant_id,
        slot_id: userSlot[0].id,
        slot_time: selected_date2,
        quantity: (userSlot[0].capacity - tableConsumed),
      });

      const userBooking = await bookings.create({
        slot_id: userSlot[0].id,
        customer_id: customer_id,
        customer_name: customer.name,
        contact_number: customer.contact_number,
        book_date: selected_date2,
        num_guests: nguests,
      });
      return res.json({
        booking_id: userBooking.id,
      });
    } else {// if inventory has details about the slot 
 
      const totalQuantity = restaurantInventory[0].quantity - tableConsumed;

      if (totalQuantity >= 0) {
        const conditions = {
          slot_id: userSlot[0].id,
        };
        const updates = {
          quantity: Sequelize.literal(`${totalQuantity}`),
      };

        await inventory.update(updates, {
          where: conditions,
        });

        const userBooking = await bookings.create({
          slot_id: userSlot[0].id,
          customer_id: customer_id,
          customer_name: customer.name,
          contact_number: customer.contact_number,
          book_date: selected_date2,
          num_guests: nguests,
        });
        return res.json({
          booking_id: userBooking.id,
        });
      } else {// if quantity is less than 0 for that slot
        return res.json({
          message:
            "The restaurant is full for this timeslot choose other slot.",
        });
      }
    }
  } catch (err) {
    console.log("Error executing query:", err);
    res.status(500).json({ error: "Internal Server Error" });
  }
}

module.exports = handleSavingSlot;
