const moment = require("moment");
const inventory = require("../models").inventory;
const Sequelize = require("sequelize");


async function handleAvailableSlot(req, res) {
  try {
    const pageValue = parseInt(req.query.page) || 1;
    const limitValue = parseInt(req.query.limit) || 5;
    const offsetValue = (pageValue - 1) * limitValue;

    const get_data = req.body;
    const restaurant_id = get_data.restaurant_id;
    const selected_date = String(get_data.selected_date);
    const nguests = get_data.nguests;

    const currentTime = String(
      moment().format("YYYY-MM-DD HH:mm:ss").slice(11, 16)
    );
    //console.log(currentTime)
    const currentDate = String(moment().format("YYYY-MM-DD"));

    if (selected_date < currentDate) {
      return res.json({ message: "Wrong Date Book in future" });
    }

    const inventoryResults = await inventory.findAll({
      attributes: [
        [Sequelize.fn("TIME", Sequelize.col("slot_time")), "slot_time"], // Extract time from slot_time
        [Sequelize.fn("SUM", Sequelize.col("quantity")), "total_quantity"],
      ],
      where: {
        restaurant_id: restaurant_id,
        [Sequelize.Op.and]: [
          Sequelize.literal(`DATE(slot_time) = '${selected_date}'`), // Compare only the date part
        ],
      },
      group: [Sequelize.fn("TIME", Sequelize.col("slot_time"))], // Group by the extracted time
      raw: true,
    });

    const tableConsumed = Math.ceil(nguests / 4);
    const finalInventoryResults = inventoryResults.filter(
      (result) => parseInt(result.total_quantity) + tableConsumed > 6
    );

    // const inventoryList = inventoryResults.map((row) =>
    //   moment(row.start_time).format("HH:mm")
    // );

    const availableSlots = [
      "01:00-02:00",
      "02:00-03:00",
      "03:00-04:00",
      "04:00-05:00",
      "05:00-06:00",
      "06:00-07:00",
      "07:00-08:00",
      "08:00-09:00",
      "09:00-10:00",
      "10:00-11:00",
      "11:00-12:00",
      "13:00-14:00",
      "14:00-15:00",
      "15:00-16:00",
      "16:00-17:00",
      "17:00-18:00",
      "18:00-19:00",
      "19:00-20:00",
      "20:00-21:00",
      "21:00-22:00",
      "22:00-23:00",
      "23:00-00:00",
    ]; // break of 1 hour at 12 midnight and 12 noon

    const resultList = finalInventoryResults.map((row) =>
      row.slot_time.slice(0, 5)
    );
    console.log(finalInventoryResults);
    console.log(resultList);

    let remainingSlots;
    if (selected_date === currentDate) {
      remainingSlots = availableSlots.filter((slot) => {
        const slotStartTime = slot.slice(0, 5); // Extract 'HH:mm' part
        return (
          !resultList.includes(slotStartTime) && slotStartTime > currentTime
        );
      });
    } else {
      remainingSlots = availableSlots.filter(
        (slot) => !resultList.includes(slot.split("-")[0])
      );
    }

    console.log({ checkdate: selected_date, remainingSlots });

    const totalPages = Math.ceil(remainingSlots.length / limitValue);
    const response = {
      checkdate: selected_date,
      page: pageValue,
      limit: limitValue,
      offset: offsetValue,
      totalPages: totalPages,
      remainingSlots: remainingSlots.slice(
        offsetValue,
        offsetValue + limitValue
      ),
    };

    return res.json(response);
  } catch (error) {
    console.log("Error querying database:", error);
    res.status(500).json({ error: "Internal server error" });
  }
}

module.exports = handleAvailableSlot;
