const moment = require("moment");
const inventory = require("../models").inventory;
const slot = require("../models").slots;
const {Sequelize} = require("sequelize");


async function handleAvailableSlotB(req, res) {
  try {
    // pagination
    const pageValue = parseInt(req.query.page) || 1;
    const limitValue = parseInt(req.query.limit) || 5;
    const offsetValue = (pageValue - 1) * limitValue;


    // req.body parameters
    const get_data = req.body;
    const restaurant_id = get_data.restaurant_id;
    const selected_date = String(get_data.selected_date);
    const nguests = get_data.nguests;


    // current datetime
    const currentTime = String(
      moment().format("YYYY-MM-DD HH:mm:ss").slice(11, 16)
    );
    const currentDate = String(moment().format("YYYY-MM-DD"));


    // cant show slots of yesterday  
    if (selected_date < currentDate) {
      return res.json({ message: "Wrong Date Book in future" });
    }

    // checking slots of selected date
    const slotResults = await slot.findAll({
        attributes: [
            [Sequelize.fn("TIME", Sequelize.col("start_time")), "start_time"],
            [Sequelize.fn("TIME", Sequelize.col("end_time")), "end_time"],
          ],
        where: {
            restaurant_id:restaurant_id,
            [Sequelize.Op.and]: [
                Sequelize.literal(`DATE(start_time) = '${selected_date}'`), // Compare only the date part
              ],
        }
    }); // if no slots is available for that day
    if (slotResults.length == 0){
        return res.json({
            message: "No slots are available for that day"
        })
    }

    // all the available slots from slot table
    const availableSlots = slotResults.map((row) => {
    const startTime = row.start_time.slice(0, 5);
    const endTime = row.end_time.slice(0, 5);
    return `${startTime}-${endTime}`;
});
    const availableSlotsSet = new Set(availableSlots);
    console.log({availableSlotsSet});

    console.log("SLot,,,.,.")


    // checking the inventory for all time slots and its quantity
    const inventoryResults = await inventory.findAll({
      attributes: [
        [Sequelize.fn("TIME", Sequelize.col("slot_time")), "slot_time"],
        "quantity" // Extract time from slot_time
    ],
      where: {
        restaurant_id: restaurant_id,
        [Sequelize.Op.and]: [
          Sequelize.literal(`DATE(slot_time) = '${selected_date}'`), // Compare only the date part
        ],
      },

    });

    console.log("inv,,.,..");

    // assuming restaurants have four sitting tables
    const tableConsumed = Math.ceil(nguests / 4);

    // storing all the non-available slots for removing
    const finalInventoryResults = inventoryResults.filter(
      (result) => parseInt(result.quantity) - tableConsumed < 0
      
    );
    
    // slicing to get only the start-time of the slot
    const resultList = finalInventoryResults.map((row) =>
      row.slot_time.slice(0, 5)
    );
    //console.log(finalInventoryResults);
    console.log({non_available_slots: resultList});


    let remainingSlots;
    if (selected_date === currentDate) {
      remainingSlots = availableSlots.filter((slot) => {
        const slotStartTime = slot.slice(0, 5); // Extract 'HH:mm' part
        return (
          !resultList.includes(slotStartTime) && slotStartTime > currentTime
        );
      });
    } else {
      remainingSlots = availableSlots.filter(
        (slot) => !resultList.includes(slot.split("-")[0])
      );
    }

    if (remainingSlots.length == 0){
      return res.json({
        message: "No Slots for the current date"
      })
    }
    console.log({ checkdate: selected_date, remainingSlots });
    
    const totalPages = Math.ceil(remainingSlots.length / limitValue);
    

    const response = {
      checkdate: selected_date,
      page: pageValue,
      limit: limitValue,
      offset: offsetValue,
      totalPages: totalPages,
      remainingSlots: remainingSlots.slice(
        offsetValue,
        offsetValue + limitValue
      ),
    };

    return res.json(response);
  } catch (error) {
    console.log("Error querying database:", error);
    res.status(500).json({ error: "Internal server error" });
  }
}

module.exports = handleAvailableSlotB;
