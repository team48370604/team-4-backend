const restaurants = require("../models").restaurants;
const Sequelize = require("sequelize");

async function handleLocation(req, res) {
  // Execute Sequelize query for distinct locations
  try {
    let pageValue = parseInt(req.body.page) || 1;
    const limitValue = parseInt(req.body.limit) || 5;
    if (pageValue <= 0) {
      pageValue = 1;
    }


    const totalRecords = await restaurants.count({
      distinct: true,
      col: "location",
    });
    console.log(totalRecords);
    const totalPages = Math.ceil(totalRecords / limitValue);
    if (pageValue >= totalPages){
      pageValue = totalPages;
    }
    const offsetValue = (pageValue - 1) * limitValue;

    const authData = req.authData;
    console.log(authData);
    const results = await restaurants.findAll({
      attributes: [
        [Sequelize.fn("DISTINCT", Sequelize.col("location")), "location"],
      ],
      offset: offsetValue,
      limit: limitValue,
    });
    

    const response = {
      message: "Success!",
      authData,
      page: pageValue,
      limit: limitValue,
      offset: offsetValue,
      totalPages: totalPages,
      results,
    };

    return res.json(response);
  } catch (err) {
    console.log("Error executing query:", err);
    return res.status(500).json({ error: "Internal Server Error" });
  }
}

module.exports = handleLocation;
