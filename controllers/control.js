// const { sha512, saltHashPassword } = require("../encryption/hashing");

// const customers = require("../models").customers;
// const restaurants = require("../models").restaurants;
// const slots = require("../models").slots;
// const bookings = require("../models").bookings;
// const inventory = require("../models").inventory;

// const jwt = require("jsonwebtoken");
// const jwtSecretKey = require("../middlewares/jwtSecretKey");

// const Sequelize = require("sequelize");

// async function handleRegistration(req, res, next) {
//   try {
//     if (
//       !req.body ||
//       !req.body.name ||
//       !req.body.email ||
//       !req.body.password ||
//       !req.body.contact_number
//     ) {
//       return res
//         .status(400)
//         .json({ message: "Bad request! All fields are required!" });
//     }

//     const { name, email, password, contact_number } = req.body;
//     console.log(name, email, password, contact_number);
//     const hash_data = saltHashPassword(password);
//     let hashPassword = hash_data.passwordHash;
//     const salt = hash_data.salt;

//     // Check if the user already exists
//     const existingUser = await customers.findOne({
//       where: {
//         email: email,
//       },
//     });

//     if (existingUser) {
//       console.log("User already exists!!!");
//       res.status(200);
//       res.json({ message: "User already exists!!!" });
//     } else {
//       // Create a new user
//       const newUser = await customers.create({
//         name: name,
//         email: email,
//         password: hashPassword, // Note: Assuming password is already hashed in this case
//         contact_number: contact_number,
//         salt: salt,
//         createdAt: new Date(),
//         updatedAt: new Date(),
//       });

//       console.log("Register successful");
//       res.status(201);
//       return res.json({ message: "Register successful" });
//     }
//   } catch (error) {
//     console.log("Error during registration:", error);
//     res.status(500);
//     return res.json({ message: "Internal Server Error" });
//   }
// }

// async function handleLogin(req, res) {
//   try {
//     if (!req.body || !req.body.email || !req.body.password) {
//       return res
//         .status(400)
//         .json({ message: "Bad request! all fields are required!" });
//     }

//     const { email, password } = req.body;

//     const user = await customers.findOne({
//       where: {
//         email: email,
//       },
//     });

//     if (!user) {
//       return res.json({ message: "User does not exist!!!" });
//     }

//     const hashedPassword = sha512(password, user.salt).passwordHash;

//     if (user.password === hashedPassword) {
//       const userDetails = {
//         cid: user.id,
//         name: user.name,
//         email: user.email,
//       };
//       jwt.sign(
//         { userDetails },
//         jwtSecretKey,
//         { expiresIn: "6000s" },
//         (err, token) => {
//           res.json({ token: token });
//         }
//       );
//     } else {
//       res.json({ message: "Wrong password!" });
//       console.log("wrong password");
//     }
//   } catch (error) {
//     console.log("Error during login:", error);
//     res.status(500).json({ message: "Internal Server Error" });
//   }
// }

// async function handleLocation(req, res) {
//   // Execute Sequelize query for distinct locations
//   try {
//     const pageValue = parseInt(req.query.page) || 1;
//     const limitValue = parseInt(req.query.limit) || 5;
//     const offsetValue = (pageValue - 1) * limitValue;

//     const authData = req.authData;
//     console.log(authData);
//     const results = await restaurants.findAll({
//       attributes: [
//         [Sequelize.fn("DISTINCT", Sequelize.col("location")), "location"],
//       ],
//       offset: offsetValue,
//       limit: limitValue,
//     });
//     const totalRecords = await restaurants.count({
//       attributes: [
//         [Sequelize.fn("DISTINCT", Sequelize.col("location")), "location"],
//       ],
//     });
//     const totalPages = Math.ceil(totalRecords / limitValue);

//     const response = {
//       message: "Success!",
//       authData,
//       page: pageValue,
//       limit: limitValue,
//       offset: offsetValue,
//       totalPages: totalPages,
//       results,
//     };

//     return res.json(response);
//   } catch (err) {
//     console.log("Error executing query:", err);
//     return res.status(500).json({ error: "Internal Server Error" });
//   }
// }

// async function handleLocations(req, res) {
//   //const authData = req.authData;
//   //console.log(authData);
//   try {
//     const pageValue = parseInt(req.query.page) || 1;
//     const limitValue = parseInt(req.query.limit) || 5;
//     const offsetValue = (pageValue - 1) * limitValue;

//     const searchLocation = req.body.searchLocation;
//     const Op = Sequelize.Op;

//     let queryOptions = {
//       attributes: [
//         [Sequelize.fn("DISTINCT", Sequelize.col("location")), "location"],
//       ],
//       offset: offsetValue,
//       limit: limitValue,
//     };

//     if (searchLocation) {
//       queryOptions.where = {
//         [Op.or]: [
//           { location: { [Op.like]: `%${searchLocation}%` } },
//           Sequelize.literal(
//             "NOT EXISTS (SELECT 1 FROM restaurants AS r WHERE r.location LIKE :searchLocation)"
//           ),
//         ],
//       };
//       queryOptions.replacements = { searchLocation: `%${searchLocation}%` };
//     }

//     const results = await restaurants.findAll(queryOptions);
//     const totalRecords = await restaurants.count(queryOptions);
//     const totalPages = Math.ceil(totalRecords / limitValue);

//     const response = {
//       page: pageValue,
//       limit: limitValue,
//       offset: offsetValue,
//       totalPages: totalPages,
//       results: results,
//     };
//     return res.json(response);
//   } catch (err) {
//     console.log("Error executing query:", err);
//     return res.status(500).json({ error: "Internal Server Error" });
//   }
// }

// async function handleRestaurants(req, res) {
//   try {
//     const pageValue = parseInt(req.query.page) || 1;
//     const limitValue = parseInt(req.query.limit) || 5;
//     const offsetValue = (pageValue - 1) * limitValue;

//     const get_data = req.body;
//     const locationName = get_data.searchLocation;

//     const results = await restaurants.findAll({
//       where: { location: locationName },
//       offset: offsetValue,
//       limit: limitValue,
//     });

//     const totalRecords = await restaurants.count({
//       where: { location: locationName },
//     });
//     const totalPages = Math.ceil(totalRecords / limitValue);
//     const response = {
//       page: pageValue,
//       limit: limitValue,
//       offset: offsetValue,
//       totalPages: totalPages,
//       results: results,
//     };
//     console.log(results);

//     res.json(response);
//   } catch (err) {
//     console.log("Error executing query:", err);
//     return res.status(500).json({ error: "Internal Server Error" });
//   }
// }

// async function handleSelectRestaurant(req, res) {
//   try {
//     const pageValue = parseInt(req.query.page) || 1;
//     const limitValue = parseInt(req.query.limit) || 5;
//     const offsetValue = (pageValue - 1) * limitValue;

//     const get_data = req.body;
//     const restaurantName = get_data.rname;

//     const results = await restaurants.findAll({
//       where: { name: restaurantName },
//       offset: offsetValue,
//       limit: limitValue,
//     });
//     const totalRecords = await restaurants.count({
//       where: { name: restaurantName },
//     });
//     const totalPages = Math.ceil(totalRecords / limitValue);
//     const response = {
//       page: pageValue,
//       limit: limitValue,
//       offset: offsetValue,
//       totalPages: totalPages,
//       results: results,
//     };
//     console.log(results);

//     res.json(response);
//   } catch (err) {
//     console.log("Error executing query:", err);
//     res.status(500).json({
//       error: "Internal Server Error",
//     });
//   }
// }

// const moment = require("moment");
// async function handleAvailableSlot(req, res) {
//   try {
//     const pageValue = parseInt(req.query.page) || 1;
//     const limitValue = parseInt(req.query.limit) || 5;
//     const offsetValue = (pageValue - 1) * limitValue;

//     const get_data = req.body;
//     const restaurant_id = get_data.restaurant_id;
//     const selected_date = String(get_data.selected_date);
//     const nguests = get_data.nguests;

//     const currentTime = String(
//       moment().format("YYYY-MM-DD HH:mm:ss").slice(11, 16)
//     );
//     //console.log(currentTime)
//     const currentDate = String(moment().format("YYYY-MM-DD"));

//     if (selected_date < currentDate) {
//       return res.json({ message: "Wrong Date Book in future" });
//     }

//     const inventoryResults = await inventory.findAll({
//       attributes: [
//         [Sequelize.fn("TIME", Sequelize.col("slot_time")), "slot_time"], // Extract time from slot_time
//         [Sequelize.fn("SUM", Sequelize.col("quantity")), "total_quantity"],
//       ],
//       where: {
//         restaurant_id: restaurant_id,
//         [Sequelize.Op.and]: [
//           Sequelize.literal(`DATE(slot_time) = '${selected_date}'`), // Compare only the date part
//         ],
//       },
//       group: [Sequelize.fn("TIME", Sequelize.col("slot_time"))], // Group by the extracted time
//       raw: true,
//     });

//     const tableConsumed = Math.ceil(nguests / 4);
//     const finalInventoryResults = inventoryResults.filter(
//       (result) => parseInt(result.total_quantity) + tableConsumed > 6
//     );

//     // const inventoryList = inventoryResults.map((row) =>
//     //   moment(row.start_time).format("HH:mm")
//     // );

//     const availableSlots = [
//       "01:00-02:00",
//       "02:00-03:00",
//       "03:00-04:00",
//       "04:00-05:00",
//       "05:00-06:00",
//       "06:00-07:00",
//       "07:00-08:00",
//       "08:00-09:00",
//       "09:00-10:00",
//       "10:00-11:00",
//       "11:00-12:00",
//       "13:00-14:00",
//       "14:00-15:00",
//       "15:00-16:00",
//       "16:00-17:00",
//       "17:00-18:00",
//       "18:00-19:00",
//       "19:00-20:00",
//       "20:00-21:00",
//       "21:00-22:00",
//       "22:00-23:00",
//       "23:00-00:00",
//     ]; // break of 1 hour at 12 midnight and 12 noon

//     const resultList = finalInventoryResults.map((row) =>
//       row.slot_time.slice(0, 5)
//     );
//     console.log(finalInventoryResults);
//     console.log(resultList);

//     let remainingSlots;
//     if (selected_date === currentDate) {
//       remainingSlots = availableSlots.filter((slot) => {
//         const slotStartTime = slot.slice(0, 5); // Extract 'HH:mm' part
//         return (
//           !resultList.includes(slotStartTime) && slotStartTime > currentTime
//         );
//       });
//     } else {
//       remainingSlots = availableSlots.filter(
//         (slot) => !resultList.includes(slot.split("-")[0])
//       );
//     }

//     console.log({ checkdate: selected_date, remainingSlots });

//     const totalPages = Math.ceil(remainingSlots.length / limitValue);
//     const response = { 
//       checkdate: selected_date, 
//       page: pageValue,
//       limit:limitValue,
//       offset: offsetValue,
//       totalPages: totalPages,
//       remainingSlots: remainingSlots.slice(offsetValue, offsetValue + limitValue),
//      };

//     return res.json(response);
//   } catch (error) {
//     console.log("Error querying database:", error);
//     res.status(500).json({ error: "Internal server error" });
//   }
// }

// async function handleSavingSlot(req, res) {
//   try {
//     let { restaurant_id, selected_date, start_time, nguests } = req.body;
//     const authData = req.authData;
//     //console.log(authData);
//     const customer_id = req.authData.userDetails.cid;

//     const selected_date2 = moment(
//       `${selected_date} ${start_time}`,
//       "YYYY-MM-DD HH:mm:ss"
//     ).format("YYYY-MM-DD HH:mm:ss");

//     start_time = selected_date2.slice(11, 16);

//     const currentTime = String(
//       moment().format("YYYY-MM-DD HH:mm:ss").slice(11, 16)
//     );
//     const currentDate = String(moment().format("YYYY-MM-DD"));

//     if (
//       selected_date < currentDate ||
//       (selected_date == currentDate && start_time < currentTime)
//     ) {
//       return res.json({ message: "Wrong Date Book in future" });
//     }
//     //assuming table has 4 seats
//     const tableConsumed = Math.ceil(nguests / 4);

//     //console.log({ restaurant_id, selected_date, start_time, capacity });
//     const restaurantInventory = await inventory.findAll({
//       where: { restaurant_id: restaurant_id, slot_time: selected_date2 },
//     });
//     //console.log(restaurantInventory);

//     if (!restaurantInventory || restaurantInventory.length === 0) {
//       //console.log("message-inside");
//       const userSlot = await slots.create({
//         restaurant_id: restaurant_id,
//         start_time: selected_date2,
//         end_time: new Date(selected_date2).setHours(
//           new Date(selected_date2).getHours() + 1
//         ),
//       });
//       const customer = await customers.findByPk(customer_id);
//       if (!customer) {
//         //throw new Error('Customer not found');
//         return res.json({
//           message: "invalid_customer",
//         });
//       }
//       await inventory.create({
//         restaurant_id: restaurant_id,
//         slot_id: userSlot.id,
//         slot_time: selected_date2,
//         quantity: tableConsumed,
//       });

//       const userBooking = await bookings.create({
//         slot_id: userSlot.id,
//         customer_id: customer_id,
//         customer_name: customer.name,
//         contact_number: customer.contact_number,
//         book_date: selected_date2,
//         num_guests: nguests,
//       });
//       return res.json({
//         booking_id: userBooking.id,
//       });
//     } else {
//       //console.log("=-=-=-=-=-=-=-=--=-");
//       const totalQuantity = restaurantInventory.reduce(
//         (sum, row) => sum + row.quantity,
//         0
//       );
//       //console.log(totalQuantity);

//       //to find slot capacity in a restaurant
//       const slot = await slots.findOne({
//         where: { id: restaurantInventory[0].slot_id },
//       });
//       // console.log(slot);
//       // console.log(slot.capacity);
//       let maxCapacity;
//       if (slot) {
//         maxCapacity = slot.capacity;
//       } else {
//         maxCapacity = 6;
//       }

//       if (totalQuantity + tableConsumed <= maxCapacity) {
//         const userSlot = await slots.create({
//           restaurant_id: restaurant_id,
//           start_time: selected_date2,
//           end_time: new Date(selected_date2).setHours(
//             new Date(selected_date2).getHours() + 1
//           ),
//         });
//         const customer = await customers.findByPk(customer_id);
//         if (!customer) {
//           //throw new Error('Customer not found');
//           return res.json({ message: "Invalid custommer" });
//         }
//         await inventory.create({
//           restaurant_id: restaurant_id,
//           slot_id: userSlot.id,
//           slot_time: selected_date2,
//           quantity: tableConsumed,
//         });

//         const userBooking = await bookings.create({
//           slot_id: userSlot.id,
//           customer_id: customer_id,
//           customer_name: customer.name,
//           contact_number: customer.contact_number,
//           book_date: selected_date2,
//           num_guests: nguests,
//         });
//         return res.json({
//           booking_id: userBooking.id,
//         });
//       } else {
//         return res.json({
//           message:
//             "The restaurant is full for this timeslot choose other slot.",
//         });
//       }
//     }
//   } catch (err) {
//     console.log("Error executing query:", err);
//     res.status(500).json({ error: "Internal Server Error" });
//   }
// }

// async function handleFinalDetails(req, res) {
//   try {
//     const bid = req.body.id;

//     const result = await bookings.findByPk(bid);

//     if (!result) {
//       return res.json({ message: "Booking not found" });
//     }
//     console.log(result.id);
//     res.json(result);
//   } catch (err) {
//     console.log("Error executing query:", err);
//     res.status(500).json({ error: "Internal Server Error" });
//   }
// }
// module.exports = {
//   handleRegistration,
//   handleLogin,
//   handleLocation,
//   handleLocations,
//   handleRestaurants,
//   handleSelectRestaurant,
//   handleAvailableSlot,
//   handleSavingSlot,
//   handleFinalDetails,
// };
