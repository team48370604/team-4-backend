const { sha512 } = require("../encryption/hashing");

const customers = require("../models").customers;

const jwt = require("jsonwebtoken");
const jwtSecretKey = require("../middlewares/jwtSecretKey");

const Sequelize = require("sequelize");


async function handleLogin(req, res) {
    try {
      if (!req.body || !req.body.email || !req.body.password) {
        return res
          .status(400)
          .json({ message: "Bad request! all fields are required!" });
      }
  
      const { email, password } = req.body;
  
      const user = await customers.findOne({
        where: {
          email: email,
        },
      });
  
      if (!user) {
        return res.json({ message: "User does not exist!!!" });
      }
  
      const hashedPassword = sha512(password, user.salt).passwordHash;
  
      if (user.password === hashedPassword) {
        const userDetails = {
          cid: user.id,
          name: user.name,
          email: user.email,
        };
        jwt.sign(
          { userDetails },
          jwtSecretKey,
          { expiresIn: "6000s" },
          (err, token) => {
            res.json({ token: token });
          }
        );
      } else {
        res.json({ message: "Wrong password!" });
        console.log("wrong password");
      }
    } catch (error) {
      console.log("Error during login:", error);
      res.status(500).json({ message: "Internal Server Error" });
    }
  }

module.exports = handleLogin;  