const restaurants = require("../models").restaurants;



async function handleRestaurants(req, res) {
    try {

      const get_data = req.body;

      const pageValue = parseInt(get_data.page) || 1;
      const limitValue = parseInt(get_data.limit) || 5;
      const offsetValue = (pageValue - 1) * limitValue;
      const locationName = get_data.location;
  
      const results = await restaurants.findAll({
        where: { location: locationName },
        offset: offsetValue,
        limit: limitValue,
      });
  
      const totalRecords = await restaurants.count({
        where: { location: locationName },
      });
      const totalPages = Math.ceil(totalRecords / limitValue);
      const response = {
        page: pageValue,
        limit: limitValue,
        offset: offsetValue,
        totalPages: totalPages,
        results: results,
      };
      console.log(results);
  
      res.json(response);
    } catch (err) {
      console.log("Error executing query:", err);
      return res.status(500).json({ error: "Internal Server Error" });
    }
  }

module.exports = handleRestaurants;