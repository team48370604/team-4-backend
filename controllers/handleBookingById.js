const bookings = require("../models").bookings;


async function handleBookingById(req, res) {
  try {
    const authData = req.authData;
    const customer_id = authData.userDetails.cid;

    const result = await bookings.findAll({
        where: {
          customer_id: customer_id
        }
      });

    if (!result || result.length == 0) {
      return res.json({ message: "Booking not found" });
    }
    res.json(result);
  } catch (err) {
    console.log("Error executing query:", err);
    res.status(500).json({ error: "Internal Server Error" });
  }
}

module.exports = handleBookingById;
