const customers = require("../models").customers;
const slots = require("../models").slots;
const bookings = require("../models").bookings;
const inventory = require("../models").inventory;

const moment = require("moment");


async function handleSavingSlot(req, res) {
    try {
      let { restaurant_id, selected_date, start_time, nguests } = req.body;
      const authData = req.authData;
      //console.log(authData);
      const customer_id = req.authData.userDetails.cid;
  
      const selected_date2 = moment(
        `${selected_date} ${start_time}`,
        "YYYY-MM-DD HH:mm:ss"
      ).format("YYYY-MM-DD HH:mm:ss");
  
      start_time = selected_date2.slice(11, 16);
  
      const currentTime = String(
        moment().format("YYYY-MM-DD HH:mm:ss").slice(11, 16)
      );
      const currentDate = String(moment().format("YYYY-MM-DD"));
  
      if (
        selected_date < currentDate ||
        (selected_date == currentDate && start_time < currentTime)
      ) {
        return res.json({ message: "Wrong Date Book in future" });
      }
      //assuming table has 4 seats
      const tableConsumed = Math.ceil(nguests / 4);
  
      //console.log({ restaurant_id, selected_date, start_time, capacity });
      const restaurantInventory = await inventory.findAll({
        where: { restaurant_id: restaurant_id, slot_time: selected_date2 },
      });
      //console.log(restaurantInventory);
  
      if (!restaurantInventory || restaurantInventory.length === 0) {
        //console.log("message-inside");
        const userSlot = await slots.create({
          restaurant_id: restaurant_id,
          start_time: selected_date2,
          end_time: new Date(selected_date2).setHours(
            new Date(selected_date2).getHours() + 1
          ),
        });
        const customer = await customers.findByPk(customer_id);
        if (!customer) {
          //throw new Error('Customer not found');
          return res.json({
            message: "invalid_customer",
          });
        }
        await inventory.create({
          restaurant_id: restaurant_id,
          slot_id: userSlot.id,
          slot_time: selected_date2,
          quantity: tableConsumed,
        });
  
        const userBooking = await bookings.create({
          slot_id: userSlot.id,
          customer_id: customer_id,
          customer_name: customer.name,
          contact_number: customer.contact_number,
          book_date: selected_date2,
          num_guests: nguests,
        });
        return res.json({
          booking_id: userBooking.id,
        });
      } else {
        //console.log("=-=-=-=-=-=-=-=--=-");
        const totalQuantity = restaurantInventory.reduce(
          (sum, row) => sum + row.quantity,
          0
        );
        //console.log(totalQuantity);
  
        //to find slot capacity in a restaurant
        const slot = await slots.findOne({
          where: { id: restaurantInventory[0].slot_id },
        });
        // console.log(slot);
        // console.log(slot.capacity);
        let maxCapacity;
        if (slot) {
          maxCapacity = slot.capacity;
        } else {
          maxCapacity = 6;
        }
  
        if (totalQuantity + tableConsumed <= maxCapacity) {
          const userSlot = await slots.create({
            restaurant_id: restaurant_id,
            start_time: selected_date2,
            end_time: new Date(selected_date2).setHours(
              new Date(selected_date2).getHours() + 1
            ),
          });
          const customer = await customers.findByPk(customer_id);
          if (!customer) {
            //throw new Error('Customer not found');
            return res.json({ message: "Invalid custommer" });
          }
          await inventory.create({
            restaurant_id: restaurant_id,
            slot_id: userSlot.id,
            slot_time: selected_date2,
            quantity: tableConsumed,
          });
  
          const userBooking = await bookings.create({
            slot_id: userSlot.id,
            customer_id: customer_id,
            customer_name: customer.name,
            contact_number: customer.contact_number,
            book_date: selected_date2,
            num_guests: nguests,
          });
          return res.json({
            booking_id: userBooking.id,
          });
        } else {
          return res.json({
            message:
              "The restaurant is full for this timeslot choose other slot.",
          });
        }
      }
    } catch (err) {
      console.log("Error executing query:", err);
      res.status(500).json({ error: "Internal Server Error" });
    }
  }

  module.exports = handleSavingSlot;