const { saltHashPassword } = require("../encryption/hashing");
const customers = require("../models").customers;
const { sequelize, Sequelize } = require("../models");

async function handleRegistration(req, res, next) {
  const t = await sequelize.transaction({
    isolationLevel: Sequelize.Transaction.ISOLATION_LEVELS.READ_COMMITTED, // Set isolation level to READ COMMITTED prevents dirty reads
  });

  try {
    if (
      !req.body ||
      !req.body.name ||
      !req.body.email ||
      !req.body.password ||
      !req.body.contact_number
    ) {
      return res
        .status(400)
        .json({ message: "Bad request! All fields are required!" });
    }

    const { name, email, password, contact_number } = req.body;
    console.log(name, email, password, contact_number);
    const hash_data = saltHashPassword(password);
    let hashPassword = hash_data.passwordHash;
    const salt = hash_data.salt;

    // Check if the user already exists
    const existingUser = await customers.findOne(
      {
        where: {
          email: email,
        },
      },
      {
        transaction: t,
      }
    );

    if (existingUser) {
      console.log("User already exists!!!");
      res.status(200);
      res.json({ message: "User already exists!!!" });
    } else {
      // Create a new user
      const newUser = await customers.create({
        name: name,
        email: email,
        password: hashPassword, // Note: Assuming password is already hashed in this case
        contact_number: contact_number,
        salt: salt,
        createdAt: new Date(),
        updatedAt: new Date(),
      },{
        transaction:t,
        lock:true
      });

      t.commit();

      console.log("Register successful");
      res.status(201);
      return res.json({ message: "Register successful" });
    }
  } catch (error) {
    t.rollback();
    console.log("Error during registration:", error);
    res.status(500);
    return res.json({ message: "Internal Server Error" });
  }
}

module.exports = handleRegistration;
