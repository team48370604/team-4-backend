const bookings = require("../models").bookings;


async function handleFinalDetails(req, res) {
  try {
    const bid = req.body.booking_id;

    const result = await bookings.findByPk(bid);

    if (!result) {
      return res.json({ message: "Booking not found" });
    }
    console.log(result.id);
    res.json(result);
  } catch (err) {
    console.log("Error executing query:", err);
    res.status(500).json({ error: "Internal Server Error" });
  }
}

module.exports = handleFinalDetails;
