const restaurants = require("../models").restaurants;
const Sequelize = require("sequelize");



async function handleSelectRestaurant(req, res) {
    try {
      const get_data = req.body;
      const pageValue = parseInt(get_data.page) || 1;
      const limitValue = parseInt(get_data.limit) || 5;
      const offsetValue = (pageValue - 1) * limitValue;
  
      const restaurantName = get_data.rname;

      const Op = Sequelize.Op;
  
      let queryOptions = {
        attributes: [
          [Sequelize.fn("DISTINCT", Sequelize.col("name")), "name"],
        ],
        offset: offsetValue,
        limit: limitValue,
      };
  
      if (restaurantName) {
        queryOptions.where = {
          [Op.or]: [
            { name: { [Op.like]: `${restaurantName}%` } },
            Sequelize.literal(
              "NOT EXISTS (SELECT 1 FROM restaurants AS r WHERE r.name LIKE :restaurantName)"
            ),
          ],
        };
        queryOptions.replacements = { restaurantName: `${restaurantName}%` };
      }
  
      const results = await restaurants.findAll(queryOptions);
  
      // const results = await restaurants.findAll({
      //   where: { name: restaurantName },
      //   offset: offsetValue,
      //   limit: limitValue,
      // });

      // const totalRecords = await restaurants.count({
      //   where: { name: restaurantName },
      // });
      // const totalPages = Math.ceil(totalRecords / limitValue);
      const response = {
        page: pageValue,
        limit: limitValue,
        offset: offsetValue,
        // totalPages: totalPages,
        results: results,
      };
      console.log(results);
  
      res.json(response);
    } catch (err) {
      console.log("Error executing query:", err);
      res.status(500).json({
        error: "Internal Server Error",
      });
    }
  }
module.exports = handleSelectRestaurant;  