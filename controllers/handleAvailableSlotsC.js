const moment = require("moment");
const inventory = require("../models").inventory;
const slot = require("../models").slots;
const { Sequelize } = require("sequelize");

async function handleAvailableSlotC(req, res) {
  try {
    const get_data = req.body;
    // pagination
    const pageValue = parseInt(get_data.page) || 1;
    const limitValue = parseInt(get_data.limit) || 5;
    const offsetValue = (pageValue - 1) * limitValue;

    // req.body parameters
    const restaurant_id = get_data.restaurant_id;
    const selected_date = String(get_data.selected_date);

    // current datetime
    const currentTime = String(
      moment().format("YYYY-MM-DD HH:mm:ss").slice(11, 16)
    );
    const currentDate = String(moment().format("YYYY-MM-DD"));

    // cant show slots of yesterday
    if (selected_date < currentDate) {
      return res.json({ message: "Wrong Date Book in future" });
    }
    const inventoryResults = await inventory.findAll({
      attributes: [
        "slot_id",
        "quantity",
      ],
      where: {
        restaurant_id: restaurant_id,
        slot_time: selected_date,
        quantity: {
          [Sequelize.Op.gt]: 0, // Check if table_consumed is greater than 0
        },
      },
    });
    const slotListQuantity = {}
    const slotList = inventoryResults.map((row) => {
        slotListQuantity[row.slot_id] = row.quantity;
        return row.slot_id
    });

    

    const slotResults = await slot.findAll({
      attributes: ["id","start_time", "end_time"],
      where: {
        restaurant_id: restaurant_id,
        id: {
          [Sequelize.Op.in]: slotList, // Check if slot_id is in slotList array
        },
      },
    });

    // all the available slots from slot table
    const availableSlots = slotResults.map((row) => {
      const startTime = row.start_time.slice(0, 5);
      const endTime = row.end_time.slice(0, 5);
      const quantity = slotListQuantity[row.id];
      return [`${startTime}-${endTime}`,quantity];
    });

    let remainingSlots;
    if (selected_date === currentDate) {
      remainingSlots = availableSlots.filter((slot) => {
        const slotStartTime = slot[0].slice(0, 5); // Extract 'HH:mm' part
        return slotStartTime > currentTime;
      });
    } else {
      remainingSlots = availableSlots.filter((slot) => {
        return slot;
      });
    }

    if (remainingSlots.length == 0) {
      return res.json({
        message: "No Slots for the current date",
      });
    }
    console.log({ checkdate: selected_date, remainingSlots });

    const totalPages = Math.ceil(remainingSlots.length / limitValue);
    const response = {
      checkdate: selected_date,
      page: pageValue,
      limit: limitValue,
      offset: offsetValue,
      totalPages: totalPages,
      remainingSlots: remainingSlots.slice(
        offsetValue,
        offsetValue + limitValue
      ),
    };

    return res.json(response);
  } catch (error) {
    console.log("Error querying database:", error);
    res.status(500).json({ error: "Internal server error" });
  }
}

module.exports = handleAvailableSlotC;
