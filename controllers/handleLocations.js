const restaurants = require("../models").restaurants;
const Sequelize = require("sequelize");

async function handleLocations(req, res) {
    //const authData = req.authData;
    //console.log(authData);
    try {
      let pageValue = parseInt(req.body.page) || 1;
      if (pageValue<=0){
        pageValue = 1
      }
      const limitValue = parseInt(req.body.limit) || 5;
      const offsetValue = (pageValue - 1) * limitValue;

      
  
      const searchLocation = req.body.search_location;
      const Op = Sequelize.Op;
  
      let queryOptions = {
        attributes: [
          [Sequelize.fn("DISTINCT", Sequelize.col("location")), "location"],
        ],
        offset: offsetValue,
        limit: limitValue,
      };
  
      if (searchLocation) {
        queryOptions.where = {
          [Op.or]: [
            { location: { [Op.like]: `${searchLocation}%` } },
            Sequelize.literal(
              "NOT EXISTS (SELECT 1 FROM restaurants AS r WHERE r.location LIKE :searchLocation)"
            ),
          ],
        };
        queryOptions.replacements = { searchLocation: `%${searchLocation}%` };
      }
  
      const results = await restaurants.findAll(queryOptions);
      if (!results || results.length == 0){
        return res.json({
          page: pageValue,
          limit: limitValue,
          message: "empty page go to previous"
        })
      }

      const response = {
        page: pageValue,
        limit: limitValue,
        offset: offsetValue,
        results: results,
      };
      return res.json(response);
    } catch (err) {
      console.log("Error executing query:", err);
      return res.status(500).json({ error: "Internal Server Error" });
    }
  }

module.exports = handleLocations;  