const express = require("express");

const router = express.Router();

const verifyToken = require("../middlewares/tokenVerify");

// Controllers
const handleRegistration = require("../controllers/handleRegistration");
const handleLogin = require("../controllers/handleLogin");
const handleLocation = require("../controllers/handleLocation");
const handleLocations = require("../controllers/handleLocations");
const handleRestaurants = require("../controllers/handleRestaurants");
const handleSelectRestaurant = require("../controllers/handleSelectRestaurant");
const handleAvailableSlot = require("../controllers/handleAvailableSlot");
const handleSavingSlot = require("../controllers/handleSavingSlot");
const handleAvailableSlotB = require("../controllers/handleAvailableSlotsB");
const handleAvailableSlotC = require("../controllers/handleAvailableSlotsC");
const handleFinalDetails = require("../controllers/handleFinalDetails");
const handleSavingSlotB = require( "../controllers/handleSavingSlotB") ;
const handleSavingSlotC = require( "../controllers/handleSavingSlotC") ;
const handleBookingById = require("../controllers/handleBookingById");

// Validations
const validateRegister = require("../middlewares/validations/registrationValidation");
const validateLogin = require("../middlewares/validations/loginValidation");
const validateLocation = require("../middlewares/validations/locationValidation");
const validateRestaurant = require("../middlewares/validations/restaurantValidation");
const validateAvailableSlot = require("../middlewares/validations/slotValidation");
const validateBookings = require("../middlewares/validations/bookingsValidations");
const validateFinalDetails = require("../middlewares/validations/finalDetailsValidation");

//registration
router.post("/api/register", validateRegister, handleRegistration);

//login
router.post("/api/login", validateLogin, handleLogin);

// search location apip
router.post("/api/location", verifyToken, handleLocation);

// // search  location  api
router.post("/api/locations", verifyToken, validateLocation, handleLocations);

// // restaurant list api
router.post(
  "/api/restaurants",
  verifyToken,
  validateLocation,
  handleRestaurants
);

// // select restaurant api
router.post(
  "/api/selectRestaurant",
  verifyToken,
  validateRestaurant,
  handleSelectRestaurant
);

// // slot availability and booking
router.post(
  "/api/availableSlot",
  verifyToken,
  validateAvailableSlot,
  handleAvailableSlotC
);

// //inserting slot entry
router.post("/api/savingSlot", verifyToken, validateBookings, handleSavingSlotC);

// // get final details
router.post(
  "/api/finalDetails",
  verifyToken,
  validateFinalDetails,
  handleFinalDetails
);

router.post(
  "/api/findBookings",
  verifyToken,
  handleBookingById
);

module.exports = router;
