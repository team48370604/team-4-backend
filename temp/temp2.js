const  handleAvailableSlotB = require("../../controllers/handleAvailableSlotsB");
const { inventory } = require("../../models");
const moment = require("moment");
const {Sequelize} = require("sequelize");

//jest.mock('moment', () => () => ({ format: jest.fn(), subtract: jest.fn(), add: jest.fn() }));
jest.mock('../../models', () => ({
  inventory: { findAll: jest.fn() },
  slots: { findAll: jest.fn() },
}));

jest.mock('sequelize', () => ({ Op: { and: jest.fn(), gt: jest.fn() } }));


describe('handleAvailableSlotB', () => {
  // Test case 1: Describe the behavior when selected_date is before the current date
  test('Returns "Wrong Date Book in future" message for past dates', async () => {
    const req = {
      query: {},
      body: {
        restaurant_id: 1,
        selected_date: '2022-01-01',
        nguests: 4,
      },
    };
    const res = {
      json: jest.fn(),
    };

    await handleAvailableSlotB(req, res);

    expect(res.json).toHaveBeenCalledWith({ message: 'Wrong Date Book in future' });
  });
  test('Returns available slots when inventoryResults is not empty', async () => {
    const req = {
      query: { page: 1, limit: 5 },
      body: { restaurant_id: 1, selected_date: '2024-02-16', nguests: 4 },
    };
  
    inventory.findAll.mockResolvedValue([
      { slot_time: '2024-02-16 12:00:00', quantity: 5 },
    ]);
  
    const res = { json: jest.fn(), status: jest.fn().mockReturnThis() }; // Mocking status function
  
    await handleAvailableSlotB(req, res);
  
    expect(res.json).toHaveBeenCalledWith(expect.objectContaining({
      checkdate: '2024-02-16', // Ensure date format matches the controller's expectation
      remainingSlots: expect.any(Array),
    }));
  });
  
  test('Returns available slots when inventoryResults is empty', async () => {
    inventory.findAll.mockResolvedValue([]);
  
    const req = {
      query: { page: 1, limit: 5 },
      body: { restaurant_id: 1, selected_date: '2024-02-16', nguests: 4 },
    };
  
    const res = { json: jest.fn(), status: jest.fn().mockReturnThis() }; // Mocking status function
  
    await handleAvailableSlotB(req, res);
  
    expect(res.json).toHaveBeenCalledWith(expect.objectContaining({
      checkdate: '2024-02-16', // Ensure date format matches the controller's expectation
      remainingSlots: expect.any(Array),
    }));
  });

  // Add more test cases as needed
});
