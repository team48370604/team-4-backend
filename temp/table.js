const handleAvailableSlotB = require("./handleAvailableSlotB");
const inventory = require("../models").inventory;
const slot = require("../models").slots;
const sequelize = require("sequelize");
const moment = require("moment");

jest.mock("../models");
jest.mock("moment");

describe("handleAvailableSlotB", () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  it("should return an error if the selected date is in the past", async () => {
    const req = {
      query: {
        page: 1,
        limit: 5,
      },
      body: {
        restaurant_id: 1,
        selected_date: moment().subtract(1, "days").toDate(),
        nguests: 4,
      },
    };
    const res = { json: jest.fn() };

    moment.format.mockReturnValue("2022-03-24 15:30:00");

    await handleAvailableSlotB(req, res);

    expect(res.json).toHaveBeenCalledWith({ message: "Wrong Date Book in future" });
  });

  it("should return an error if there are no slots available for the selected date", async () => {
    const req = {
      query: {
        page: 1,
        limit: 5,
      },
      body: {
        restaurant_id: 1,
        selected_date: moment().toDate(),
        nguests: 4,
      },
    };
    const res = { json: jest.fn() };

    slot.findAll.mockResolvedValue([]);

    await handleAvailableSlotB(req, res);

    expect(res.json).toHaveBeenCalledWith({ message: "No slots are available for that day" });
  });

  it("should return the available slots for the selected date", async () => {
    const req = {
      query: {
        page: 1,
        limit: 5,
      },
      body: {
        restaurant_id: 1,
        selected_date: moment().toDate(),
        nguests: 4,
      },
    };
    const res = { json: jest.fn() };

    const slotResults = [
      { start_time: "2022-03-24 16:00:00", end_time: "2022-03-24 17:00:00" },
      { start_time: "2022-03-24 18:00:00", end_time: "2022-03-24 19:00:00" },
    ];

    slot.findAll.mockResolvedValue(slotResults);

    await handleAvailableSlotB(req, res);

    expect(res.json).toHaveBeenCalledWith({
      checkdate: moment().format("YYYY-MM-DD"),
      page: 1,
      limit: 5,
      offset: 0,
      totalPages: 1,
      remainingSlots: ["16:00-17:00", "18:00-19:00"],
    });
  });

  it("should return an error if there are no remaining slots for the selected date", async () => {
    const req = {
      query: {
        page: 1,
        limit: 5,
      },
      body: {
        restaurant_id: 1,
        selected_date: moment().toDate(),
        nguests: 10,
      },
    };
    const res = { json: jest.fn() };

    const slotResults = [
      { start_time: "2022-03-24 16:00:00", end_time: "2022-03-24 17:00:00" },
      { start_time: "2022-03-24 18:00:00", end_time: "2022-03-24 19:00:00" },
    ];

    const inventoryResults = [
      { slot_time: "2022-03-24 16:00:00", quantity: 4 },
      { slot_time: "2022-03-24 18:00:00", quantity: 4 },
    ];

    slot.findAll.mockResolvedValue(slotResults);
    inventory.findAll.mockResolvedValue(inventoryResults);

    await handleAvailableSlotB(req, res);

    expect(res.json).toHaveBeenCalledWith({
      message: "No Slots for the current date",
    });
  });
});